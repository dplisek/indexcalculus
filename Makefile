default: all

define SUBDIRS
( cd FiniteSparseLU; $(MAKE) $@ )
endef

all: 
	$(SUBDIRS)

clean: 
	$(SUBDIRS)
