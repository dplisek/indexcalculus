/*
 * luFactor.c
 *
 *  Created on: Mar 10, 2017
 *      Author: plech
 */

#include <FiniteSparseLU.h>
#include <SolveLU.h>
#include <stdio.h>
#include <stdlib.h>
#include <inttypes.h>
#include <Debug.h>
#include <Utils.h>

int allocDemoMemory(MatrixIndex **A3i, MatrixIndex **A3j, Value ***A3x, Value ***b, Value **modules, ValueIndex valueCount, int moduleCount, MatrixIndex rowCount) {
	int mod;

	if (!(*A3i = safe_malloc(valueCount, sizeof(MatrixIndex)))) return 1;
	if (!(*A3j = safe_malloc(valueCount, sizeof(MatrixIndex)))) return 1;
	if (!(*A3x = safe_calloc(moduleCount, sizeof(Value *)))) return 1;
	for (mod = 0; mod < moduleCount; ++mod) {
		if (!((*A3x)[mod] = safe_malloc(valueCount, sizeof(Value)))) return 1;
	}
	if (!(*b = safe_calloc(moduleCount, sizeof(Value *)))) return 1;
	for (mod = 0; mod < moduleCount; ++mod) {
		if (!((*b)[mod] = safe_malloc(rowCount, sizeof(Value)))) return 1;
	}
	if (!(*modules = safe_malloc(moduleCount, sizeof(Value)))) return 1;
	return 0;
}

void freeDemoMemory(

		Value **modules, Value ***A3x, Value ***Lx, Value ***Ux, Value ***b,
		MatrixIndex **A3i, MatrixIndex **A3j, MatrixIndex **Li, MatrixIndex **Ui, MatrixIndex **rowPerm, MatrixIndex **colPerm, MatrixIndex **rowPermInv, MatrixIndex **colPermInv,
		ValueIndex **Lp, ValueIndex **Up,
		FILE **file,
		int moduleCount

		) {

	int mod;

	*modules = safe_free(*modules);
	*A3x = safe_free_incl_sublevel((void **)*A3x, moduleCount);
	*Lx = safe_free_incl_sublevel((void **)*Lx, moduleCount);
	*Ux = safe_free_incl_sublevel((void **)*Ux, moduleCount);
	*b = safe_free_incl_sublevel((void **)*b, moduleCount);
	*A3i = safe_free(*A3i);
	*A3j = safe_free(*A3j);
	*Li = safe_free(*Li);
	*Ui = safe_free(*Ui);
	*rowPerm = safe_free(*rowPerm);
	*colPerm = safe_free(*colPerm);
	*rowPermInv = safe_free(*rowPermInv);
	*colPermInv = safe_free(*colPermInv);
	*Lp = safe_free(*Lp);
	*Up = safe_free(*Up);
	*file = safe_fclose(*file);
}

int main(int argc, char **argv) {
	Value *modules = 0, **A3x = 0, **Lx = 0, **Ux = 0, **b = 0, temp;
	MatrixIndex *A3i = 0, *A3j = 0, *Li = 0, *Ui = 0, *rowPerm = 0, *colPerm = 0, *rowPermInv = 0, *colPermInv = 0, rowCount, colCount, i;
	ValueIndex *Lp = 0, *Up = 0, value, valueCount;
	FILE *file = 0;
	int mod, moduleCount;

	if (argc < 7) {
		printf("Usage: luSolveDemo <matrix input file> <b input file> <value count> <row count> <col count> <modules...>\n\n");
		printf("input file\tTriplet form input matrix. One line represents one value of sparse matrix. Values cannot exceed 2^64 - 1.\n");
		printf("\tLine format: <row index> <col index> <value>\n\n");
		printf("b input file\tRight-hand side. Values cannot exceed 2^64 - 1.\n\n");
		printf("value count\tNumber of values in the sparse matrix. Cannot exceed 2^64 - 1.\n\n");
		printf("row count\tNumber of rows. Can be more than number of columns, if some rows are linear dependent. Cannot exceed 2^32 - 1.\n\n");
		printf("col count\tNumber of columns. Has to fit in 32-bit unsigned integer. Cannot exceed 2^32 - 1.\n\n");
		printf("modules\t\tList of modules to work in separately, none can exceed 2^32.\n\n");
		return 0;
	}

	if (!sscanf(argv[3], "%"PRIu64, &valueCount)) {
		fprintf(stderr, "Incorrectly specified value count %s. Exiting.\n", argv[3]);
		return 1;
	}

	if (!sscanf(argv[4], "%"PRIu64, &rowCount)) {
		fprintf(stderr, "Incorrectly specified row count %s. Exiting.\n", argv[4]);
		return 1;
	}

	if (!sscanf(argv[5], "%"PRIu64, &colCount)) {
		fprintf(stderr, "Incorrectly specified column count %s. Exiting.\n", argv[5]);
		return 1;
	}

	moduleCount = argc - 6;

	if (allocDemoMemory(&A3i, &A3j, &A3x, &b, &modules, valueCount, moduleCount, rowCount)) {
		fprintf(stderr, "Cannot allocate enough memory. Exiting.\n");
		freeDemoMemory(&modules, &A3x, &Lx, &Ux, &b, &A3i, &A3j, &Li, &Ui, &rowPerm, &colPerm, &rowPermInv, &colPermInv, &Lp, &Up, &file, moduleCount);
		return 1;
	}

	for (mod = 0; mod < moduleCount; ++mod) {
		if (!sscanf(argv[mod + 6], "%"PRIu64, modules + mod)) {
			fprintf(stderr, "Incorrectly specified module nr. %d (%s). Exiting.\n", mod, argv[mod + 6]);
			freeDemoMemory(&modules, &A3x, &Lx, &Ux, &b, &A3i, &A3j, &Li, &Ui, &rowPerm, &colPerm, &rowPermInv, &colPermInv, &Lp, &Up, &file, moduleCount);
			return 1;
		}
	}

	file = fopen(argv[1], "r");
	if (!file) {
		fprintf(stderr, "Cannot open matrix file %s. Exiting.\n", argv[1]);
		freeDemoMemory(&modules, &A3x, &Lx, &Ux, &b, &A3i, &A3j, &Li, &Ui, &rowPerm, &colPerm, &rowPermInv, &colPermInv, &Lp, &Up, &file, moduleCount);
		return 1;
	}
	for (value = 0; value < valueCount; ++value) {
		if (fscanf(file, "%"PRIu64" %"PRIu64" %"PRIu64, A3i + value, A3j + value, &temp) != 3) {
			fprintf(stderr, "Cannot read value nr. %"PRIu64" from matrix file.\n", value);
			freeDemoMemory(&modules, &A3x, &Lx, &Ux, &b, &A3i, &A3j, &Li, &Ui, &rowPerm, &colPerm, &rowPermInv, &colPermInv, &Lp, &Up, &file, moduleCount);
			return 1;
		}
		for (mod = 0; mod < moduleCount; ++mod) {
			A3x[mod][value] = temp % modules[mod];
		}
	}
	file = safe_fclose(file);

	file = fopen(argv[2], "r");
	if (!file) {
		fprintf(stderr, "Cannot open right-hand side file %s. Exiting.\n", argv[2]);
		freeDemoMemory(&modules, &A3x, &Lx, &Ux, &b, &A3i, &A3j, &Li, &Ui, &rowPerm, &colPerm, &rowPermInv, &colPermInv, &Lp, &Up, &file, moduleCount);
		return 1;
	}
	for (i = 0; i < rowCount; ++i) {
		if (fscanf(file, "%"PRIu64, &temp) != 1) {
			fprintf(stderr, "Cannot read value nr. %"PRIu64" from right-hand side file.\n", i);
			freeDemoMemory(&modules, &A3x, &Lx, &Ux, &b, &A3i, &A3j, &Li, &Ui, &rowPerm, &colPerm, &rowPermInv, &colPermInv, &Lp, &Up, &file, moduleCount);
			return 1;
		}
		for (mod = 0; mod < moduleCount; ++mod) {
			b[mod][i] = temp % modules[mod];
		}
	}
	file = safe_fclose(file);

	if (finiteSparseLU(
			modules, moduleCount,
			A3i, A3j, A3x,
			rowCount, colCount, valueCount,
			&Lp, &Li, &Lx,
			&Up, &Ui, &Ux,
			&rowPerm, &rowPermInv,
			&colPerm, &colPermInv
			)) {

		fprintf(stderr, "Error during LU factorization. Exiting.\n");
		freeDemoMemory(&modules, &A3x, &Lx, &Ux, &b, &A3i, &A3j, &Li, &Ui, &rowPerm, &colPerm, &rowPermInv, &colPermInv, &Lp, &Up, &file, moduleCount);
		return 1;
	}

	file = fopen("mathematica_check.nb", "w");
	if (!file) {
		fprintf(stderr, "Cannot open output mathematica file for writing. Exiting.\n");
		freeDemoMemory(&modules, &A3x, &Lx, &Ux, &b, &A3i, &A3j, &Li, &Ui, &rowPerm, &colPerm, &rowPermInv, &colPermInv, &Lp, &Up, &file, moduleCount);
		return 1;
	}
	fprintf(file, "P=");
	printRowPermMathematicaFriendly(file, rowPerm, colCount);
	fprintf(file, "Q=");
	printColPermMathematicaFriendly(file, colPerm, colCount);
	fprintf(file, "A=L=U=b=x=ConstantArray[0,%d];\n", moduleCount);
	for (mod = 0; mod < moduleCount; ++mod) {
		fprintf(file, "A[[%d]]=", mod + 1);
		printSparseMathematicaFriendly(file, A3i, A3j, A3x[mod], valueCount);
		fprintf(file, "L[[%d]]=", mod + 1);
		printCompressedColSquareSparseMathematicaFriendly(file, Lp, Li, Lx[mod], colCount);
		fprintf(file, "U[[%d]]=", mod + 1);
		printCompressedRowSparseMathematicaFriendly(file, Up, Ui, Ux[mod], colCount);
		fprintf(file, "LU=Mod[L[[%d]].U[[%d]], %"PRIu64"];\n", mod + 1, mod + 1, modules[mod]);
		fprintf(file, "PAQ=Mod[P.A[[%d]].Q, %"PRIu64"];\n", mod + 1, modules[mod]);
		fprintf(file, "LU==PAQ\n");
	}

	for (mod = 0; mod < moduleCount; ++mod) {
		fprintf(file, "b[[%d]]=", mod + 1);
		printVectorMathematicaFriendly(file, b[mod], rowCount);
	}

	if (solveLU(
			modules, moduleCount,
			Lp, Li, Lx,
			Up, Ui, Ux,
			rowCount, colCount,
			rowPerm, rowPermInv,
			colPerm,
			b
			)) {

		fprintf(stderr, "Couldn't solve linear system. Exiting.\n");
		freeDemoMemory(&modules, &A3x, &Lx, &Ux, &b, &A3i, &A3j, &Li, &Ui, &rowPerm, &colPerm, &rowPermInv, &colPermInv, &Lp, &Up, &file, moduleCount);
		return 1;
	}

	for (mod = 0; mod < moduleCount; ++mod) {
		fprintf(file, "x[[%d]]=", mod + 1);
		printVectorMathematicaFriendly(file, b[mod], colCount);
		fprintf(file, "Mod[A[[%d]].x[[%d]], %"PRIu64"] == Mod[b[[%d]], %"PRIu64"]\n", mod + 1, mod + 1, modules[mod], mod + 1, modules[mod]);
		fprintf(file, "P.Mod[A[[%d]].x[[%d]], %"PRIu64"] == P.Mod[b[[%d]], %"PRIu64"]\n", mod + 1, mod + 1, modules[mod], mod + 1, modules[mod]);
	}
	freeDemoMemory(&modules, &A3x, &Lx, &Ux, &b, &A3i, &A3j, &Li, &Ui, &rowPerm, &colPerm, &rowPermInv, &colPermInv, &Lp, &Up, &file, moduleCount);
	return 0;
}
