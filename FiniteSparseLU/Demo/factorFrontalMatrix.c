/*
 * singleFrontalMatrix.fragment.cu
 *
 *  Created on: Mar 3, 2017
 *      Author: plech
 */

#include <FactorFrontal.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <inttypes.h>
#include <math.h>

#define MODULE_LOWER_BOUND	10000000

int isPrime(Value number) {
	Value test;
	for (test = 2; test <= sqrt(number); ++test) {
		if (number % test == 0) {
			return 0;
		}
	}
	return 1;
}

void fillModules(TaskDescriptor *taskQueue, int tasks) {
	int i;
	Value module;

	module = MODULE_LOWER_BOUND;
	for (i = 0; i < tasks; ++i) {
		while (!isPrime(module)) ++module;
		taskQueue[i].module = module;
#ifdef DEBUG
		printf("%"PRIu64" ", module);
#endif
		++module;
	}
#ifdef DEBUG
	printf("\n");
#endif
}

void generatePivotBlock(TaskDescriptor *task, int index, FILE *file, int pivots) {
	int m, n;

	task->pivotBlock = malloc(pivots * pivots * sizeof(Value));
	fprintf(file, "P[[%d]] = {", index + 1);
	for (m = 0; m < pivots; ++m) {
		if (m > 0) fprintf(file, ",");
		fprintf(file, "\n{");
		for (n = 0; n < pivots; ++n) {
			task->pivotBlock[m * pivots + n] = rand() % task->module;
			fprintf(file, "%010"PRIu64, task->pivotBlock[m * pivots + n]);
			if (n < pivots - 1) fprintf(file, ", ");
		}
		fprintf(file, "}");
	}
	fprintf(file, "\n};\n");
}

void generatePivotRowBlock(TaskDescriptor *task, int index, FILE *file, int pivots, MatrixIndex colCount) {
	int m, n;

	task->pivotRowBlock = malloc(pivots * (colCount - pivots) * sizeof(Value));
	fprintf(file, "Q[[%d]] = {", index + 1);
	for (m = 0; m < pivots; ++m) {
		if (m > 0) fprintf(file, ",");
		fprintf(file, "\n{");
		for (n = 0; n < (colCount - pivots); ++n) {
			task->pivotRowBlock[m * (colCount - pivots) + n] = rand() % task->module;
			fprintf(file, "%010"PRIu64, task->pivotRowBlock[m * (colCount - pivots) + n]);
			if (n < (colCount - pivots) - 1) fprintf(file, ", ");
		}
		fprintf(file, "}");
	}
	fprintf(file, "\n};\n");
}

void generatePivotColBlock(TaskDescriptor *task, int index, FILE *file, int pivots, MatrixIndex rowCount) {
	int m, n;

	task->pivotColBlock = malloc((rowCount - pivots) * pivots * sizeof(Value));
	fprintf(file, "R[[%d]] = {", index + 1);
	for (m = 0; m < rowCount - pivots; ++m) {
		if (m > 0) fprintf(file, ",");
		fprintf(file, "\n{");
		for (n = 0; n < pivots; n += 1) {
			task->pivotColBlock[m * pivots + n] = rand() % task->module;
			fprintf(file, "%010"PRIu64, task->pivotColBlock[m * pivots + n]);
			if (n < pivots - 1) fprintf(file, ", ");
		}
		fprintf(file, "}");
	}
	fprintf(file, "\n};\n");
}

void generateContributionBlock(TaskDescriptor *task, int index, FILE *file, int pivots, MatrixIndex rowCount, MatrixIndex colCount) {
	int m, n;

	task->contributionBlock = malloc((rowCount - pivots) * (colCount - pivots) * sizeof(Value));
	fprintf(file, "Cold[[%d]] = {", index + 1);
	for (m = 0; m < rowCount - pivots; ++m) {
		if (m > 0) fprintf(file, ",");
		fprintf(file, "\n{");
		for (n = 0; n < (colCount - pivots); ++n) {
			task->contributionBlock[m * (colCount - pivots) + n] = rand() % task->module;
			fprintf(file, "%010"PRIu64, task->contributionBlock[m * (colCount - pivots) + n]);
			if (n < (colCount - pivots) - 1) fprintf(file, ", ");
		}
		fprintf(file, "}");
	}
	fprintf(file, "\n};\n");
}

void printPivotBlock(TaskDescriptor *task, int index, FILE *file, int pivots) {
	int m, n;

	fprintf(file, "L1U1[[%d]] = {", index + 1);
	for (m = 0; m < pivots; ++m) {
		if (m > 0) fprintf(file, ",");
		fprintf(file, "\n{");
		for (n = 0; n < pivots; ++n) {
			fprintf(file, "%010"PRIu64, task->pivotBlock[m * pivots + n]);
			if (n < pivots - 1) fprintf(file, ", ");
		}
		fprintf(file, "}");
	}
	fprintf(file, "\n};\n");

	fprintf(file, "L[[%d]] = {", index + 1);
	for (m = 0; m < pivots; ++m) {
		if (m > 0) fprintf(file, ",");
		fprintf(file, "\n{");
		for (n = 0; n < m; ++n) {
			fprintf(file, "%010"PRIu64, task->pivotBlock[m * pivots + n]);
			if (n < pivots - 1) fprintf(file, ", ");
		}
		fprintf(file, "%010ld", 1L);
		if (m < pivots - 1) fprintf(file, ", ");
		for (n = m + 1; n < pivots; ++n) {
			fprintf(file, "%010ld", 0L);
			if (n < pivots - 1) fprintf(file, ", ");
		}
		fprintf(file, "}");
	}
	fprintf(file, "\n};\n");

	fprintf(file, "U[[%d]] = {", index + 1);
	for (m = 0; m < pivots; ++m) {
		if (m > 0) fprintf(file, ",");
		fprintf(file, "\n{");
		for (n = 0; n < m; ++n) {
			fprintf(file, "%010ld", 0L);
			if (n < pivots - 1) fprintf(file, ", ");
		}
		for (n = m; n < pivots; ++n) {
			fprintf(file, "%010"PRIu64, task->pivotBlock[m * pivots + n]);
			if (n < pivots - 1) fprintf(file, ", ");
		}
		fprintf(file, "}");
	}
	fprintf(file, "\n};\n");

	free(task->pivotBlock);
}

void printPivotRowBlock(TaskDescriptor *task, int index, FILE *file, int pivots, MatrixIndex colCount) {
	int m, n;

	fprintf(file, "U2[[%d]] = {", index + 1);
	for (m = 0; m < pivots; ++m) {
		if (m > 0) fprintf(file, ",");
		fprintf(file, "\n{");
		for (n = 0; n < (colCount - pivots); n += 1) {
			fprintf(file, "%010"PRIu64, task->pivotRowBlock[m * (colCount - pivots) + n]);
			if (n < (colCount - pivots) - 1) fprintf(file, ", ");
		}
		fprintf(file, "}");
	}
	fprintf(file, "\n};\n");

	free(task->pivotRowBlock);
}

void printPivotColBlock(TaskDescriptor *task, int index, FILE *file, int pivots, MatrixIndex rowCount) {
	int m, n;

	fprintf(file, "L2[[%d]] = {", index + 1);
	for (m = 0; m < rowCount - pivots; ++m) {
		if (m > 0) fprintf(file, ",");
		fprintf(file, "\n{");
		for (n = 0; n < pivots; n += 1) {
			fprintf(file, "%010"PRIu64, task->pivotColBlock[m * pivots + n]);
			if (n < pivots - 1) fprintf(file, ", ");
		}
		fprintf(file, "}");
	}
	fprintf(file, "\n};\n");

	free(task->pivotColBlock);
}

void printContributionBlock(TaskDescriptor *task, int index, FILE *file, int pivots, MatrixIndex rowCount, MatrixIndex colCount) {
	int m, n;

	fprintf(file, "Cnew[[%d]] = {", index + 1);
	for (m = 0; m < rowCount - pivots; ++m) {
		if (m > 0) fprintf(file, ",");
		fprintf(file, "\n{");
		for (n = 0; n < (colCount - pivots); n += 1) {
			fprintf(file, "%010"PRIu64, task->contributionBlock[m * (colCount - pivots) + n]);
			if (n < (colCount - pivots) - 1) fprintf(file, ", ");
		}
		fprintf(file, "}");
	}
	fprintf(file, "\n};\n");

	free(task->contributionBlock);
}

int main(int argc, char **argv) {
	FILE *file = fopen("mathematica_check.nb", "w");
	TaskDescriptor *taskQueue;
	int i, result, tasks, pivots;
	MatrixIndex rowCount, colCount;

	if (argc < 5) {
		printf("Usage: factorFrontalMatrix <rows> <columns> <pivots> <tasks>\n\n");
		printf("rows\tNumber of rows of the dense frontal matrix.\n\n");
		printf("columns\tNumber of columns of the dense frontal matrix.\n\n");
		printf("pivots\tNumber of pivotal rows and columns. Must be 16 or less.\n\n");
		printf("tasks\tNumber of different tasks to solve simultanously.\n\n");
		return 0;
	}

	if (!sscanf(argv[1], "%"PRIu64, &rowCount)) {
		fprintf(stderr, "Incorrectly specified row count %s. Exiting.\n", argv[1]);
		return 1;
	}
	if (!sscanf(argv[2], "%"PRIu64, &colCount)) {
		fprintf(stderr, "Incorrectly specified column count %s. Exiting.\n", argv[2]);
		return 1;
	}
	if (!sscanf(argv[3], "%d", &pivots) || pivots > 16 || pivots < 0) {
		fprintf(stderr, "Incorrectly specified pivot count %s. Exiting.\n", argv[3]);
		return 1;
	}
	if (!sscanf(argv[4], "%d", &tasks) || tasks < 0) {
		fprintf(stderr, "Incorrectly specified task count %s. Exiting.\n", argv[4]);
		return 1;
	}

	fprintf(file, "P=Q=R=Cold=L1U1=L=U=U2=L2=Cnew=ConstantArray[0, %d];\n\n", tasks);

	taskQueue = malloc(tasks * sizeof(TaskDescriptor));
	fillModules(taskQueue, tasks);

	for (i = 0; i < tasks; ++i) {
		taskQueue[i].contributionBlockRows = (rowCount - pivots);
		taskQueue[i].contributionBlockCols = (colCount - pivots);
		taskQueue[i].pivotCount = pivots;
		generatePivotBlock(taskQueue + i, i, file, pivots);
		generatePivotRowBlock(taskQueue + i, i, file, pivots, colCount);
		generatePivotColBlock(taskQueue + i, i, file, pivots, rowCount);
		generateContributionBlock(taskQueue + i, i, file, pivots, rowCount, colCount);
	}

	result = factorFrontalMatrix(taskQueue, tasks);

	if (result) {
		fprintf(stderr, "Factorization failed at pivot %d.\n", result);
	} else {
		for (i = 0; i < tasks; ++i) {
			printPivotBlock(taskQueue + i, i, file, pivots);
			printPivotRowBlock(taskQueue + i, i, file, pivots, colCount);
			printPivotColBlock(taskQueue + i, i, file, pivots, rowCount);
			printContributionBlock(taskQueue + i, i, file, pivots, rowCount, colCount);
			fprintf(file, "Mod[L[[%d]].U[[%d]], %010"PRIu64"] == P[[%d]]\n", i + 1, i + 1, taskQueue[i].module, i + 1);
			fprintf(file, "Mod[L[[%d]].U2[[%d]], %010"PRIu64"] == Q[[%d]]\n", i + 1, i + 1, taskQueue[i].module, i + 1);
			fprintf(file, "Mod[L2[[%d]].U[[%d]], %010"PRIu64"] == R[[%d]]\n", i + 1, i + 1, taskQueue[i].module, i + 1);
			fprintf(file, "Mod[Cold[[%d]]-L2[[%d]].U2[[%d]], %010"PRIu64"] == Cnew[[%d]]\n", i + 1, i + 1, i + 1, taskQueue[i].module, i + 1);
		}
	}

	fclose(file);
	free(taskQueue);

	return 0;
}
