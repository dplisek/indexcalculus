#!/bin/bash

./factorFrontalMatrix 100 100 16 100 > size/100
./factorFrontalMatrix 200 200 16 100 > size/200
./factorFrontalMatrix 300 300 16 100 > size/300
./factorFrontalMatrix 400 400 16 100 > size/400
./factorFrontalMatrix 500 500 16 100 > size/500
./factorFrontalMatrix 600 600 16 100 > size/600
./factorFrontalMatrix 700 700 16 100 > size/700
./factorFrontalMatrix 800 800 16 100 > size/800
./factorFrontalMatrix 900 900 16 100 > size/900
./factorFrontalMatrix 1000 1000 16 100 > size/1000

./factorFrontalMatrix 1000 1000 16 100 > shape/1000
./factorFrontalMatrix 2000 500 16 100 > shape/2000
./factorFrontalMatrix 4000 250 16 100 > shape/4000
./factorFrontalMatrix 8000 125 16 100 > shape/8000
./factorFrontalMatrix 20000 50 16 100 > shape/20000

./factorFrontalMatrix 1000 1000 1 100 > pivots/1
./factorFrontalMatrix 1000 1000 2 100 > pivots/2
./factorFrontalMatrix 1000 1000 3 100 > pivots/3
./factorFrontalMatrix 1000 1000 4 100 > pivots/4
./factorFrontalMatrix 1000 1000 5 100 > pivots/5
./factorFrontalMatrix 1000 1000 6 100 > pivots/6
./factorFrontalMatrix 1000 1000 7 100 > pivots/7
./factorFrontalMatrix 1000 1000 8 100 > pivots/8
./factorFrontalMatrix 1000 1000 9 100 > pivots/9
./factorFrontalMatrix 1000 1000 10 100 > pivots/10
./factorFrontalMatrix 1000 1000 11 100 > pivots/11
./factorFrontalMatrix 1000 1000 12 100 > pivots/12
./factorFrontalMatrix 1000 1000 13 100 > pivots/13
./factorFrontalMatrix 1000 1000 14 100 > pivots/14
./factorFrontalMatrix 1000 1000 15 100 > pivots/15
./factorFrontalMatrix 1000 1000 16 100 > pivots/16

./factorFrontalMatrix 1000 1000 16 10 > tasks/10
./factorFrontalMatrix 1000 1000 16 20 > tasks/20
./factorFrontalMatrix 1000 1000 16 30 > tasks/30
./factorFrontalMatrix 1000 1000 16 40 > tasks/40
./factorFrontalMatrix 1000 1000 16 50 > tasks/50
./factorFrontalMatrix 1000 1000 16 60 > tasks/60
./factorFrontalMatrix 1000 1000 16 70 > tasks/70
./factorFrontalMatrix 1000 1000 16 80 > tasks/80
./factorFrontalMatrix 1000 1000 16 90 > tasks/90
./factorFrontalMatrix 1000 1000 16 100 > tasks/100
