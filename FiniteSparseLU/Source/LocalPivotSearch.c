/*
 * LocalPivotSearch.c
 *
 *  Created on: Mar 17, 2017
 *      Author: plech
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "LocalPivotSearch.h"
#include "MinHeap.h"
#include "ElementLists.h"
#include "Utils.h"

int findLocalCandidateCol(MatrixIndex *U, MatrixIndex Ucount, Bool *Upivotal, MatrixIndex *dc, MatrixIndex *pivCol, MatrixIndex *pivColLocal, int pivots) {
	MatrixIndex j;
	int found;

	if (!(Ucount - pivots)) return 1;
	found = 0;
	for (j = 0; j < Ucount; ++j) {
		if (Upivotal[j]) continue;
		if (found && dc[U[j]] > dc[*pivCol]) continue;
		*pivColLocal = j;
		*pivCol = U[j];
		found = 1;
	}
	return 0;
}

int findLocalCandidateRow(MatrixIndex *L, MatrixIndex Lcount, Bool *Lpivotal, MatrixIndex *dr, MatrixIndex *pivRow, MatrixIndex *pivRowLocal, int pivots) {
	MatrixIndex i;
	int found;

	if (!(Lcount - pivots)) return 1;
	found = 0;
	for (i = 0; i < Lcount; ++i) {
		if (Lpivotal[i]) continue;
		if (found && dr[L[i]] > dr[*pivRow]) continue;
		*pivRowLocal = i;
		*pivRow = L[i];
		found = 1;
	}
	return 0;
}

int assembleLocalCandidateCol(Value ***F, MatrixIndex **L, Value ***Xj, MatrixIndex *Lcount, MatrixIndex pivCol, MatrixIndex pivColLocal,
		SparseMatVal **AkCols, ElementListNode **colElementLists, Element **elements,
		MatrixIndex *colHeap, MatrixIndex *colHeapInv, MatrixIndex *dc, MatrixIndex size, int localPivots, MatrixIndex globalPivots, Value *modules, int moduleCount) {

	SparseMatVal *val;
	MatrixIndex i;
	int mod;
	Bool ok;

	*L = safe_realloc(*L, dc[pivCol] + localPivots, sizeof(MatrixIndex), &ok);
	if (!ok) return 1;
	for (mod = 0; mod < moduleCount; ++mod) {
		(*Xj)[mod] = safe_realloc((*Xj)[mod], dc[pivCol] + localPivots, sizeof(Value), &ok);
		if (!ok) return 1;
		memset((*Xj)[mod] + *Lcount, 0, (dc[pivCol] + localPivots - *Lcount) * sizeof(Value));
	}

	for (i = 0; i < *Lcount; ++i) {
		for (mod = 0; mod < moduleCount; ++mod) {
			(*Xj)[mod][i] = F[mod][i][pivColLocal];
		}
	}
	val = AkCols[pivCol];
	while (val) {
		for (i = 0; i < *Lcount; ++i) {
			if ((*L)[i] != val->i) continue;
			break;
		}
		if (i == *Lcount) {
			(*L)[i] = val->i;
			++(*Lcount);
		}
		for (mod = 0; mod < moduleCount; ++mod) {
			(*Xj)[mod][i] = ((*Xj)[mod][i] + val->x[mod]) % modules[mod];
		}
		val = val->nextInCol;
	}
	assembleElementCols(elements, colElementLists + pivCol, *L, Lcount, *Xj, 1, 0, modules, moduleCount);
	dc[pivCol] = *Lcount - localPivots;
	updateColCandidate(colHeap, colHeapInv, size - globalPivots, pivCol, dc);

	return 0;
}

int assembleLocalCandidateRow(Value ***F, MatrixIndex **U, Value ***Xi, MatrixIndex *Ucount, MatrixIndex pivRow, MatrixIndex pivRowLocal,
		SparseMatVal **AkRows, ElementListNode **rowElementLists, Element **elements, MatrixIndex *dr, int pivots, Value *modules, int moduleCount) {

	SparseMatVal *val;
	MatrixIndex j;
	int mod;
	Bool ok;

	*U = safe_realloc(*U, dr[pivRow] + pivots, sizeof(MatrixIndex), &ok);
	if (!ok) return 1;
	for (mod = 0; mod < moduleCount; ++mod) {
		(*Xi)[mod] = safe_realloc((*Xi)[mod], dr[pivRow] + pivots, sizeof(Value), &ok);
		if (!ok) return 1;
		memset((*Xi)[mod] + *Ucount, 0, (dr[pivRow] + pivots - *Ucount) * sizeof(Value));
	}

	for (j = 0; j < *Ucount; ++j) {
		for (mod = 0; mod < moduleCount; ++mod) {
			(*Xi)[mod][j] = F[mod][pivRowLocal][j];
		}
	}
	val = AkRows[pivRow];
	while (val) {
		for (j = 0; j < *Ucount; ++j) {
			if ((*U)[j] != val->j) continue;
			break;
		}
		if (j == *Ucount) {
			(*U)[j] = val->j;
			++(*Ucount);
		}
		for (mod = 0; mod < moduleCount; ++mod) {
			(*Xi)[mod][j] = ((*Xi)[mod][j] + val->x[mod]) % modules[mod];
		}
		val = val->nextInRow;
	}
	assembleElementRows(elements, rowElementLists + pivRow, *U, Ucount, *Xi, 1, 0, modules, moduleCount, 0, 0);
	dr[pivRow] = *Ucount - pivots;

	return 0;
}
