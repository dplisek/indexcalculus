#include "Utils.h"

Value calculateInverse(Value input, Value module) {
	Value u, v, x1, x2, r, x, q;

	// Extended Euclid
	// u and v start at <= module and are decremented throughout, staying positive, no need for moduling
	// x1 and x2 jump all around the field, need to module them

	u = input;
	v = module;
	x1 = 1;
	x2 = 0;
	while (u != 1) {
		q = v / u;
		r = v - q * u;
		x = (q * x1) % module;
		x = (module + x2 - x) % module; // prepend module so that subtraction doesn't go negative
		v = u;
		u = r;
		x2 = x1;
		x1 = x;
	}
	return x1;
}

void elapsedTimeStart(Time *time) {
	gettimeofday(&time->t1, 0);
}

void elapsedTimeEnd(Time *time) {
	gettimeofday(&time->t2, 0);
	time->elapsed.tv_sec += time->t2.tv_sec - time->t1.tv_sec;
	time->elapsed.tv_usec += time->t2.tv_usec - time->t1.tv_usec;
}

FILE *safe_fclose(FILE *f) {
	if (f) fclose(f);
	return 0;
}

void *safe_free_incl_sublevel(void **p, int count) {
	int i;

	if (p) {
		for (i = 0; i < count; ++i) {
			p[i] = safe_free(p[i]);
		}
		p = safe_free(p);
	}
	return 0;
}

// The following wrappers are copyright of Tim Davis

/* wrapper for malloc */
void *safe_malloc (size_t n, size_t size)
{
	size_t total = MAX(n * size, 1);
    return (malloc (total)) ;
}

/* wrapper for calloc */
void *safe_calloc (size_t n, size_t size)
{
    return (calloc (MAX (n,1), size)) ;
}

/* wrapper for free */
void *safe_free (void *p)
{
    if (p) free (p) ;       /* free p if it is not already NULL */
    return 0 ;         		/* return NULL to simplify the use of cs_free */
}

/* wrapper for realloc */
void *safe_realloc (void *p, size_t n, size_t size, Bool *ok)
{
    void *pnew ;
    pnew = realloc (p, MAX (n,1) * size) ; 	/* realloc the block */
    *ok = (pnew != NULL) ;                  /* realloc fails if pnew is NULL */
    return ((*ok) ? pnew : p) ;             /* return original p if failure */
}
