/*
 * PrepareFactorize.c
 *
 *  Created on: Mar 24, 2017
 *      Author: plech
 */

#include "FactorizeWrapper.h"
#include "Debug.h"
#include "Utils.h"
#include "ElementLists.h"
#include <FactorFrontal.h>
#include <stdio.h>
#include <stdlib.h>
#include <inttypes.h>

int allocateFactorizeMemory(TaskDescriptor **taskQueue, MatrixIndex **origColIndices, MatrixIndex **origRowIndices, int pivots, MatrixIndex Lcount, MatrixIndex Ucount, int moduleCount) {
	if (!(*taskQueue = safe_calloc(moduleCount, sizeof(TaskDescriptor)))) return 1;
	if (!(*origRowIndices = safe_malloc(Lcount - pivots, sizeof(MatrixIndex)))) return 1;
	if (!(*origColIndices = safe_malloc(Ucount - pivots, sizeof(MatrixIndex)))) return 1;
	return 0;
}

void freeFactorizeWorkspaceMemory(TaskDescriptor **taskQueue, MatrixIndex **origColIndices, MatrixIndex **origRowIndices, int moduleCount) {
	int mod;

	if (*taskQueue) {
		for (mod = 0; mod < moduleCount; ++mod) {
			(*taskQueue)[mod].pivotBlock = safe_free((*taskQueue)[mod].pivotBlock);
			(*taskQueue)[mod].pivotRowBlock = safe_free((*taskQueue)[mod].pivotRowBlock);
			(*taskQueue)[mod].pivotColBlock = safe_free((*taskQueue)[mod].pivotColBlock);
			(*taskQueue)[mod].contributionBlock = safe_free((*taskQueue)[mod].contributionBlock);
		}
	}
	*taskQueue = safe_free(*taskQueue);
	*origRowIndices = safe_free(*origRowIndices);
	*origColIndices = safe_free(*origColIndices);
}

void freeAllFactorizeMemory(TaskDescriptor **taskQueue, MatrixIndex **origColIndices, MatrixIndex **origRowIndices, int moduleCount, Element **element) {
	freeFactorizeWorkspaceMemory(taskQueue, origColIndices, origRowIndices, moduleCount);
	*element = freeElement(*element, moduleCount);
}

int fillTaskQueue(TaskDescriptor *taskQueue, Value ***F, MatrixIndex *pivotRowLocal, MatrixIndex *pivotColLocal, Bool *Lpivotal, Bool *Upivotal, int pivots, MatrixIndex Lcount, MatrixIndex Ucount, Value *modules, int moduleCount) {
	int mod;
	MatrixIndex i, j, m, n;

	for (mod = 0; mod < moduleCount; ++mod) {
		taskQueue[mod].contributionBlockRows = Lcount - pivots;
		taskQueue[mod].contributionBlockCols = Ucount - pivots;
		taskQueue[mod].module = modules[mod];
		taskQueue[mod].pivotCount = pivots;
		if (!(taskQueue[mod].pivotBlock = safe_malloc(pivots * pivots, sizeof(Value)))) return 1;
		if (!(taskQueue[mod].pivotRowBlock = safe_malloc(pivots * (Ucount - pivots), sizeof(Value)))) return 1;
		if (!(taskQueue[mod].pivotColBlock = safe_malloc((Lcount - pivots) * pivots, sizeof(Value)))) return 1;
		if (!(taskQueue[mod].contributionBlock = safe_malloc((Lcount - pivots) * (Ucount - pivots), sizeof(Value)))) return 1;
		for (i = 0; i < pivots; ++i) {
			for (j = 0; j < pivots; ++j) {
				taskQueue[mod].pivotBlock[i * pivots + j] = F[mod][pivotRowLocal[i]][pivotColLocal[j]];
			}
			j = 0;
			for (n = 0; n < Ucount; ++n) {
				if (Upivotal[n]) continue;
				taskQueue[mod].pivotRowBlock[i * (Ucount - pivots) + j] = F[mod][pivotRowLocal[i]][n];
				++j;
			}
		}
		i = 0;
		for (m = 0; m < Lcount; ++m) {
			if (Lpivotal[m]) continue;
			for (j = 0; j < pivots; ++j) {
				taskQueue[mod].pivotColBlock[i * pivots + j] = F[mod][m][pivotColLocal[j]];
			}
			j = 0;
			for (n = 0; n < Ucount; ++n) {
				if (Upivotal[n]) continue;
				taskQueue[mod].contributionBlock[i * (Ucount - pivots) + j] = F[mod][m][n];
				++j;
			}
			++i;
		}
	}
	return 0;
}

void setOrigIndices(MatrixIndex *origColIndices, MatrixIndex *origRowIndices, MatrixIndex *L, Bool *Lpivotal, MatrixIndex *U, Bool *Upivotal, MatrixIndex Lcount, MatrixIndex Ucount) {
	MatrixIndex i, j, m, n;

	j = 0;
	for (n = 0; n < Ucount; ++n) {
		if (Upivotal[n]) continue;
		origColIndices[j] = U[n];
		++j;
	}

	i = 0;
	for (m = 0; m < Lcount; ++m) {
		if (Lpivotal[m]) continue;
		origRowIndices[i] = L[m];
		++i;
	}
}

int reallocateOutput(MatrixIndex **Loutp, MatrixIndex **Louti, Value ***Loutx, MatrixIndex **Uoutp, MatrixIndex **Uouti, Value ***Uoutx, MatrixIndex Lcount, MatrixIndex Ucount, ValueIndex *allocatedLvalueCount, ValueIndex *allocatedUvalueCount, int moduleCount, MatrixIndex elementOffset, int pivots) {
	Bool ok;
	int mod;

	if (*allocatedLvalueCount < (*Loutp)[elementOffset] + pivots * Lcount) {
		*allocatedLvalueCount = ((*Loutp)[elementOffset] + pivots * Lcount) * 2;
		*Louti = safe_realloc(*Louti, *allocatedLvalueCount, sizeof(MatrixIndex), &ok);
		if (!ok) return 1;
		for (mod = 0; mod < moduleCount; ++mod) {
			(*Loutx)[mod] = safe_realloc((*Loutx)[mod], *allocatedLvalueCount, sizeof(Value), &ok);
			if (!ok) return 1;
		}
	}
	if (*allocatedUvalueCount < (*Uoutp)[elementOffset] + pivots * Ucount) {
		*allocatedUvalueCount = ((*Uoutp)[elementOffset] + pivots * Ucount) * 2;
		*Uouti = safe_realloc(*Uouti, *allocatedUvalueCount, sizeof(MatrixIndex), &ok);
		if (!ok) return 1;
		for (mod = 0; mod < moduleCount; ++mod) {
			(*Uoutx)[mod] = safe_realloc((*Uoutx)[mod], *allocatedUvalueCount, sizeof(Value), &ok);
			if (!ok) return 1;
		}
	}
	return 0;
}

void writeOutput(MatrixIndex **Loutp, MatrixIndex **Louti, Value ***Loutx, MatrixIndex **Uoutp, MatrixIndex **Uouti, Value ***Uoutx, int pivots, MatrixIndex elementOffset, Value *modules, int moduleCount, TaskDescriptor *taskQueue, MatrixIndex Lcount, MatrixIndex Ucount, MatrixIndex *origColIndices, MatrixIndex *origRowIndices) {
	MatrixIndex i, j;
	int mod;

	for (j = 0; j < pivots; ++j) {
		(*Loutp)[elementOffset + j + 1] = (*Loutp)[elementOffset + j];
		(*Louti)[(*Loutp)[elementOffset + j + 1]] = elementOffset + j;
		for (mod = 0; mod < moduleCount; ++mod) {
			(*Loutx)[mod][(*Loutp)[elementOffset + j + 1]] = 1;
		}
		++((*Loutp)[elementOffset + j + 1]);
		for (i = j + 1; i < pivots; ++i) {
			(*Louti)[(*Loutp)[elementOffset + j + 1]] = elementOffset + i;
			for (mod = 0; mod < moduleCount; ++mod) {
				(*Loutx)[mod][(*Loutp)[elementOffset + j + 1]] = taskQueue[mod].pivotBlock[i * pivots + j];
			}
			++((*Loutp)[elementOffset + j + 1]);
		}
		for (i = 0; i < (Lcount - pivots); ++i) {
			(*Louti)[(*Loutp)[elementOffset + j + 1]] = origRowIndices[i];														// orig row index used, so:
			for (mod = 0; mod < moduleCount; ++mod) {
				(*Loutx)[mod][(*Loutp)[elementOffset + j + 1]] = modules[mod] + taskQueue[mod].pivotColBlock[i * pivots + j];	// prepend module here to indicate that the index needs permutation
			}
			++((*Loutp)[elementOffset + j + 1]);
		}
	}

	for (i = 0; i < pivots; ++i) {
		(*Uoutp)[elementOffset + i + 1] = (*Uoutp)[elementOffset + i];
		for (j = i; j < pivots; ++j) {
			(*Uouti)[(*Uoutp)[elementOffset + i + 1]] = elementOffset + j;
			for (mod = 0; mod < moduleCount; ++mod) {
				(*Uoutx)[mod][(*Uoutp)[elementOffset + i + 1]] = taskQueue[mod].pivotBlock[i * pivots + j];
			}
			++((*Uoutp)[elementOffset + i + 1]);
		}
		for (j = 0; j < (Ucount - pivots); ++j) {
			(*Uouti)[(*Uoutp)[elementOffset + i + 1]] = origColIndices[j];																	// orig col. index used, so:
			for (mod = 0; mod < moduleCount; ++mod) {
				(*Uoutx)[mod][(*Uoutp)[elementOffset + i + 1]] = modules[mod] + taskQueue[mod].pivotRowBlock[i * (Ucount - pivots) + j];	// prepend module here to indicate that the index needs permutation
			}
			++((*Uoutp)[elementOffset + i + 1]);
		}
	}
}

int createElement(Element **element, MatrixIndex **origRowIndices, MatrixIndex **origColIndices, MatrixIndex Lcount, MatrixIndex Ucount, int pivots, int moduleCount, TaskDescriptor *taskQueue) {
	int mod;

	if (!(*element = safe_malloc(1, sizeof(Element)))) return 1;
	(*element)->Le = *origRowIndices;
	*origRowIndices = 0;
	(*element)->LeCount = (*element)->liveLeCount = Lcount - pivots;
	if (!((*element)->LeDeleted = safe_calloc((*element)->LeCount, sizeof(Bool)))) return 1;
	(*element)->Ue = *origColIndices;
	*origColIndices = 0;
	(*element)->UeCount = (*element)->liveUeCount = Ucount - pivots;
	if (!((*element)->UeDeleted = safe_calloc((*element)->UeCount, sizeof(Bool)))) return 1;
	if (!((*element)->contributionBlock = safe_calloc(moduleCount, sizeof(Value *)))) return 1;
	for (mod = 0; mod < moduleCount; ++mod) {
		(*element)->contributionBlock[mod] = taskQueue[mod].contributionBlock;
		taskQueue[mod].contributionBlock = 0;
	}
	return 0;
}

int fillElementLists(Element *element, ElementListNode **rowElementLists, ElementListNode **colElementLists, MatrixIndex elementOffset) {
	MatrixIndex i, j;
	ElementListNode *listNode;

	for (i = 0; i < element->LeCount; ++i) {
		if (!(listNode = safe_malloc(1, sizeof(ElementListNode)))) return 1;
		listNode->element = elementOffset;
		listNode->localIndex = i;
		if (rowElementLists[element->Le[i]]) {
			listNode->nextNode = rowElementLists[element->Le[i]];
		} else {
			listNode->nextNode = 0;
		}
		rowElementLists[element->Le[i]] = listNode;
	}
	for (j = 0; j < element->UeCount; ++j) {
		if (!(listNode = safe_malloc(1, sizeof(ElementListNode)))) return 1;
		listNode->element = elementOffset;
		listNode->localIndex = j;
		if (colElementLists[element->Ue[j]]) {
			listNode->nextNode = colElementLists[element->Ue[j]];
		} else {
			listNode->nextNode = 0;
		}
		colElementLists[element->Ue[j]] = listNode;
	}
	return 0;
}

int factorizeWrapperExec(Value ***F, MatrixIndex *L, Bool *Lpivotal, MatrixIndex *U, Bool *Upivotal, int pivots, MatrixIndex Lcount, MatrixIndex Ucount, ElementListNode **rowElementLists, ElementListNode **colElementLists, Element** elements, ValueIndex **Loutp, MatrixIndex **Louti, Value ***Loutx, ValueIndex **Uoutp, MatrixIndex **Uouti, Value ***Uoutx, Value *modules, int moduleCount, MatrixIndex elementOffset, ValueIndex *allocatedLvalueCount, ValueIndex *allocatedUvalueCount, MatrixIndex *pivotRowLocal, MatrixIndex *pivotColLocal) {
	TaskDescriptor *taskQueue = 0;
	MatrixIndex *origColIndices = 0, *origRowIndices = 0;
	int result;
#ifdef DEBUG
	int mod;
#endif

	if (allocateFactorizeMemory(&taskQueue, &origColIndices, &origRowIndices, pivots, Lcount, Ucount, moduleCount)) {
		freeAllFactorizeMemory(&taskQueue, &origColIndices, &origRowIndices, moduleCount, elements + elementOffset);
		return -1;
	}
	if (fillTaskQueue(taskQueue, F, pivotRowLocal, pivotColLocal, Lpivotal, Upivotal, pivots, Lcount, Ucount, modules, moduleCount)) {
		freeAllFactorizeMemory(&taskQueue, &origColIndices, &origRowIndices, moduleCount, elements + elementOffset);
		return -1;
	}
	setOrigIndices(origColIndices, origRowIndices, L, Lpivotal, U, Upivotal, Lcount, Ucount);
	result = factorFrontalMatrix(taskQueue, moduleCount);
	if (result) {
		freeAllFactorizeMemory(&taskQueue, &origColIndices, &origRowIndices, moduleCount, elements + elementOffset);
		return result;
	}
	if (reallocateOutput(Loutp, Louti, Loutx, Uoutp, Uouti, Uoutx, Lcount, Ucount, allocatedLvalueCount, allocatedUvalueCount, moduleCount, elementOffset, pivots)) {
		freeAllFactorizeMemory(&taskQueue, &origColIndices, &origRowIndices, moduleCount, elements + elementOffset);
		return -1;
	}
	writeOutput(Loutp, Louti, Loutx, Uoutp, Uouti, Uoutx, pivots, elementOffset, modules, moduleCount, taskQueue, Lcount, Ucount, origColIndices, origRowIndices);
#ifdef DEBUG
	for (mod = 0; mod < moduleCount; ++mod) {
		printf("For module %"PRIu64":\n", modules[mod]);
		printf("Factored pivot block:\n");
		printBlock(taskQueue[mod].pivotBlock, pivots, pivots);
		printf("Factored pivot row block:\n");
		printBlock(taskQueue[mod].pivotRowBlock, pivots, Ucount - pivots);
		printf("Factored pivot col block:\n");
		printBlock(taskQueue[mod].pivotColBlock, Lcount - pivots, pivots);
	}
#endif
	if (!(Lcount - pivots) || !(Ucount - pivots)) {
		freeAllFactorizeMemory(&taskQueue, &origColIndices, &origRowIndices, moduleCount, elements + elementOffset);
		return 0;
	}
#ifdef DEBUG
	for (mod = 0; mod < moduleCount; ++mod) {
		printf("For module %"PRIu64":\n", modules[mod]);
		printf("Factored contribution block:\n");
		printBlock(taskQueue[mod].contributionBlock, Lcount - pivots, Ucount - pivots);
	}
#endif
	if (createElement(elements + elementOffset, &origRowIndices, &origColIndices, Lcount, Ucount, pivots, moduleCount, taskQueue)) {
		freeAllFactorizeMemory(&taskQueue, &origColIndices, &origRowIndices, moduleCount, elements + elementOffset);
		return -1;
	}
	if (fillElementLists(elements[elementOffset], rowElementLists, colElementLists, elementOffset)) {
		freeAllFactorizeMemory(&taskQueue, &origColIndices, &origRowIndices, moduleCount, elements + elementOffset);
		return -1;
	}
	freeFactorizeWorkspaceMemory(&taskQueue, &origColIndices, &origRowIndices, moduleCount);
	return 0;
}
