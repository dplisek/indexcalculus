/*
 * SolveLU.c
 *
 *  Created on: Mar 30, 2017
 *      Author: plech
 */

#include "SolveLU.h"
#include "Utils.h"
#include "Debug.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <inttypes.h>

int allocSolveMemory(Value ***x, int colCount, int moduleCount) {
	int mod;

	if (!(*x = safe_calloc(moduleCount, sizeof(Value *)))) return 1;
	for (mod = 0; mod < moduleCount; ++mod) {
		if (!((*x)[mod] = safe_malloc(colCount, sizeof(Value)))) return 1;
	}
	return 0;
}

void freeSolveMemory(Value ***x, int moduleCount) {
	int mod;

	if (*x) {
		for (mod = 0; mod < moduleCount; ++mod) {
			(*x)[mod] = safe_free((*x)[mod]);
		}
		*x = safe_free(*x);
	}
}

void permute(MatrixIndex *perm, MatrixIndex *permInvCheck, Value **src, Value **dst, MatrixIndex size, int moduleCount) {
	MatrixIndex i;
	int mod;

	for (i = 0; i < size; ++i) {
		if (permInvCheck && permInvCheck[perm[i]] != i) continue;
		for (mod = 0; mod < moduleCount; ++mod) {
			dst[mod][perm[i]] = src[mod][i];
		}
	}
}

void solveLower(ValueIndex *Lp, MatrixIndex *Li, Value **Lx, Value **x, MatrixIndex size, Value *modules, int moduleCount) {
	MatrixIndex j;
	ValueIndex p;
	Value inv;
	int mod;

    for (j = 0; j < size; ++j) {
    	for (mod = 0; mod < moduleCount; ++mod) {
			inv = calculateInverse(Lx[mod][Lp[j]], modules[mod]);
			x[mod][j] = (x[mod][j] * inv) % modules[mod];
		}
        for (p = Lp[j] + 1; p < Lp[j + 1]; ++p) {
        	if (Li[p] < size) {
        		for (mod = 0; mod < moduleCount; ++mod) {
					x[mod][Li[p]] = (modules[mod] + x[mod][Li[p]] - ((Lx[mod][p] * x[mod][j]) % modules[mod])) % modules[mod];
				}
        	}
        }
    }
}

void solveUpper(ValueIndex *Up, MatrixIndex *Ui, Value **Ux, Value **x, MatrixIndex size, Value *modules, int moduleCount) {
	MatrixIndex j;
	ValueIndex p;
	Value inv;
	int mod;

    for (j = size - 1; j < size; --j) {
        for (p = Up[j] + 1; p < Up[j + 1]; ++p) {
        	for (mod = 0; mod < moduleCount; ++mod) {
				x[mod][j] = (modules[mod] + x[mod][j] - ((Ux[mod][p] * x[mod][Ui[p]]) % modules[mod])) % modules[mod];
			}
        }
        for (mod = 0; mod < moduleCount; ++mod) {
			inv = calculateInverse(Ux[mod][Up[j]], modules[mod]);
			x[mod][j] = (x[mod][j] * inv) % modules[mod];
		}
    }
}

int solveLU(Value *modules, int moduleCount, ValueIndex *Lp, MatrixIndex *Li, Value **Lx, ValueIndex *Up, MatrixIndex *Ui, Value **Ux, MatrixIndex rowCount, MatrixIndex colCount, MatrixIndex *rowPerm, MatrixIndex *rowPermInv, MatrixIndex *colPerm, Value **b) {
	Value **x = 0;
#ifdef TIME
	SolveLUTime time;
#endif

	if (moduleCount <= 0) {
		fprintf(stderr, "Module count must be at least one, not %d.\n", moduleCount);
		freeSolveMemory(&x, moduleCount);
		return 1;
	}
	if (rowCount < colCount) {
		fprintf(stderr, "The input matrix must have at least as many rows as it has columns. Incorrect values are rowCount: %"PRIu64", colCount: %"PRIu64".\n", rowCount, colCount);
		freeSolveMemory(&x, moduleCount);
		return 1;
	}
	if (allocSolveMemory(&x, colCount, moduleCount)) {
		fprintf(stderr, "Couldn't allocate memory for solution workspace.\n");
		freeSolveMemory(&x, moduleCount);
		return 1;
	}
#ifdef TIME
	memset(&time, 0, sizeof(SolveLUTime));
	elapsedTimeStart(&time.total);
	elapsedTimeStart(&time.rowPerm);
#endif
	permute(rowPermInv, rowPerm, b, x, rowCount, moduleCount);
#ifdef TIME
	elapsedTimeEnd(&time.rowPerm);
	elapsedTimeStart(&time.lower);
#endif
	solveLower(Lp, Li, Lx, x, colCount, modules, moduleCount);
#ifdef TIME
	elapsedTimeEnd(&time.lower);
	elapsedTimeStart(&time.upper);
#endif
	solveUpper(Up, Ui, Ux, x, colCount, modules, moduleCount);
#ifdef TIME
	elapsedTimeEnd(&time.upper);
	elapsedTimeStart(&time.colPerm);
#endif
	permute(colPerm, 0, x, b, colCount, moduleCount);
#ifdef TIME
	elapsedTimeEnd(&time.colPerm);
	elapsedTimeEnd(&time.total);
	printSolveTime(time);
#endif
	freeSolveMemory(&x, moduleCount);
	return 0;
}
