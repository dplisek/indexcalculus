/*
 * Debug.c
 *
 *  Created on: Mar 21, 2017
 *      Author: plech
 */

#include <inttypes.h>
#include "Debug.h"

void printSparseMathematicaFriendly(FILE *file, MatrixIndex *Ai, MatrixIndex *Aj, Value *Ax, ValueIndex valueCount) {
	ValueIndex i;

	fprintf(file, "SparseArray[{\n");
	for (i = 0; i < valueCount; ++i) {
		fprintf(file, "{%"PRIu64",%"PRIu64"}->%"PRIu64, Ai[i] + 1, Aj[i] + 1, Ax[i]);
		if (i < valueCount - 1) {
			fprintf(file, ",");
		}
	}
	fprintf(file, "}];\n\n");
}

void printCompressedColSquareSparseMathematicaFriendly(FILE *file, ValueIndex *Ap, MatrixIndex *Ai, Value *Ax, MatrixIndex size) {
	ValueIndex val;
	MatrixIndex p;
	Bool first;

	first = 1;
	fprintf(file, "SparseArray[{\n");
	for (p = 0; p < size; ++p) {
		for (val = Ap[p]; val < Ap[p + 1]; ++val) {
			if (Ai[val] < size) {
				if (first) first = 0;
				else fprintf(file, ",");
				fprintf(file, "{%"PRIu64",%"PRIu64"}->%"PRIu64, Ai[val] + 1, p + 1, Ax[val]);
			}
		}
	}
	fprintf(file, "}];\n\n");
}

void printCompressedRowSparseMathematicaFriendly(FILE *file, ValueIndex *Ap, MatrixIndex *Ai, Value *Ax, MatrixIndex size) {
	ValueIndex val;
	MatrixIndex p;

	fprintf(file, "SparseArray[{\n");
	for (p = 0; p < size; ++p) {
		for (val = Ap[p]; val < Ap[p + 1]; ++val) {
			fprintf(file, "{%"PRIu64",%"PRIu64"}->%"PRIu64, p + 1, Ai[val] + 1, Ax[val]);
			if (val < Ap[size] - 1) {
				fprintf(file, ",");
			}
		}
	}
	fprintf(file, "}];\n\n");
}

void printRowPermMathematicaFriendly(FILE *file, MatrixIndex *rowPerm, MatrixIndex size) {
	MatrixIndex i;

	fprintf(file, "SparseArray[{\n");
	for (i = 0; i < size; ++i) {
		fprintf(file, "{%"PRIu64",%"PRIu64"}->1", i + 1, rowPerm[i] + 1);
		if (i < size - 1) {
			fprintf(file, ",");
		}
	}
	fprintf(file, "}];\n\n");
}

void printColPermMathematicaFriendly(FILE *file, MatrixIndex *colPerm, MatrixIndex size) {
	MatrixIndex i;

	fprintf(file, "SparseArray[{\n");
	for (i = 0; i < size; ++i) {
		fprintf(file, "{%"PRIu64",%"PRIu64"}->1", colPerm[i] + 1, i + 1);
		if (i < size - 1) {
			fprintf(file, ",");
		}
	}
	fprintf(file, "}];\n\n");
}

void printVectorMathematicaFriendly(FILE *file, Value *x, MatrixIndex size) {
	MatrixIndex i;

	fprintf(file, "{\n");
	for (i = 0; i < size; ++i) {
		fprintf(file, "%"PRIu64, x[i]);
		if (i < size - 1) {
			fprintf(file, ",");
		}
	}
	fprintf(file, "};\n\n");
}

void printSparse(SparseMatVal **rows, SparseMatVal **cols, MatrixIndex *rowCounts, MatrixIndex *colCounts, MatrixIndex rowCount, MatrixIndex colCount, Value *modules, int moduleCount) {
	SparseMatVal *val;
	MatrixIndex i, j;
	int mod;

	printf("Printing sparse matrix by row:\n");
	for (mod = 0; mod < moduleCount; ++mod) {
		printf("For module %"PRIu64":\n", modules[mod]);
		for (i = 0; i < rowCount; ++i) {
			printf("Row %"PRIu64" (%"PRIu64"): ", i, rowCounts[i]);
			val = rows[i];
			while (val) {
				printf("(%"PRIu64", %"PRIu64", %"PRIu64", prev: %"PRIu64") ", val->i, val->j, val->x[mod], val->prevInRow ? val->prevInRow->x[mod] : 0);
				val = val->nextInRow;
			}
			printf("\n");
		}
		printf("\n\n");
	}
	printf("Printing sparse matrix by col:\n");
	for (mod = 0; mod < moduleCount; ++mod) {
		printf("For module %"PRIu64":\n", modules[mod]);
		for (j = 0; j < colCount; ++j) {
			printf("Col %"PRIu64" (%"PRIu64"): ", j, colCounts[j]);
			val = cols[j];
			while (val) {
				printf("(%"PRIu64", %"PRIu64", %"PRIu64", prev: %"PRIu64") ", val->i, val->j, val->x[mod], val->prevInCol ? val->prevInCol->x[mod] : 0);
				val = val->nextInCol;
			}
			printf("\n");
		}
		printf("\n\n");
	}
}

void printColHeap(MatrixIndex *colHeap, MatrixIndex *colHeapInv, MatrixIndex *dc, MatrixIndex heapSize, MatrixIndex totalSize) {
	MatrixIndex levelWidth, i, j;

	printf("Printing col heap:\n");
	i = 0;
	levelWidth = 1;
	while (i < heapSize) {
		for (j = 0; j < levelWidth && i < heapSize; ++j, ++i) {
			printf("(heap index %"PRIu64", col %"PRIu64", dc %"PRIu64") ", i, colHeap[i], dc[colHeap[i]]);
		}
		levelWidth *= 2;
		printf("\n");
	}
	printf("\n\n");
	printf("Printing col heap inverse:\n");
	for (i = 0; i < totalSize; ++i) {
		printf("Col %"PRIu64" is in heap at index %"PRIu64"\n", i, colHeapInv[i]);
	}
	printf("\n\n");
}

void printColCandidates(MatrixIndex *colCandidates, int count, MatrixIndex *dc) {
	MatrixIndex j;

	printf("Col candidates for seed pivot: ");
	for (j = 0; j < count; ++j) {
		printf("(col: %"PRIu64", dc: %"PRIu64") ", colCandidates[j], dc[colCandidates[j]]);
	}
	printf("\n\n");
}

void printAssembledForm(MatrixIndex *indices, Value **values, MatrixIndex size, Value *modules, int moduleCount) {
	MatrixIndex i;
	int mod;

	printf("Assembled form. Index: Value.\n");
	for (mod = 0; mod < moduleCount; ++mod) {
		printf("For module %"PRIu64":\n", modules[mod]);
		for (i = 0; i < size; ++i) {
			printf("%"PRIu64": %"PRIu64"\n", indices[i], values[mod][i]);
		}
	}
	printf("\n\n");
}

void printBlock(Value *block, MatrixIndex rows, MatrixIndex cols) {
	MatrixIndex i, j;

	for (i = 0; i < rows; ++i) {
		for (j = 0; j < cols; ++j) {
			printf("%010"PRIu64" ", block[i * cols + j]);
		}
		printf("\n");
	}
	printf("\n");
}

void printFrontal(Value ***F, MatrixIndex Lcount, MatrixIndex Ucount, Value *modules, int moduleCount) {
	MatrixIndex i, j;
	int mod;

	printf("Frontal matrix:\n");
	for (mod = 0; mod < moduleCount; ++mod) {
		printf("For module %"PRIu64":\n", modules[mod]);
		for (i = 0; i < Lcount; ++i) {
			for (j = 0; j < Ucount; ++j) {
				printf("%010"PRIu64" ", F[mod][i][j]);
			}
			printf("\n");
		}
	}
	printf("\n");
}

void printExternalDegrees(MatrixIndex *wCol, MatrixIndex *wRow, MatrixIndex wZeroShift, MatrixIndex size, Element **elements) {
	MatrixIndex i;

	printf("External col degrees:\n");
	for (i = 0; i < size; ++i) {
		if (!elements[i]) continue;
		printf("%"PRIu64": %"PRIu64"\n", i, wCol[i] >= wZeroShift ? wCol[i] - wZeroShift : elements[i]->liveLeCount);
	}
	printf("External row degrees:\n");
	for (i = 0; i < size; ++i) {
		if (!elements[i]) continue;
		printf("%"PRIu64": %"PRIu64"\n", i, wRow[i] >= wZeroShift ? wRow[i] - wZeroShift : elements[i]->liveUeCount);
	}
	printf("\n");
}

void printCompressedForm(ValueIndex *p, MatrixIndex *i, Value **x, MatrixIndex size, Value *modules, int moduleCount) {
	MatrixIndex rowCol;
	ValueIndex val;
	int mod;

	printf("Printing compressed form:\n");
	for (mod = 0; mod < moduleCount; ++mod) {
		printf("For module %"PRIu64":\n", modules[mod]);
		for (rowCol = 0; rowCol < size; ++rowCol) {
			printf("Row / Column %"PRIu64":\n", rowCol);
			for (val = p[rowCol]; val < p[rowCol + 1]; ++val) {
				printf("%"PRIu64": %"PRIu64"\n", i[val], x[mod][val]);
			}
		}
	}
	printf("\n\n");
}

void printElements(Element **elements, ElementListNode **rowElementLists, ElementListNode **colElementLists, MatrixIndex rowCount, MatrixIndex colCount, Value *modules, int moduleCount) {
	MatrixIndex i, j, k;
	ElementListNode *node;
	int mod;

	printf("Row element lists:\n");
	for (i = 0; i < rowCount; ++i) {
		printf("Row %"PRIu64": ", i);
		node = rowElementLists[i];
		while (node) {
			printf("(%"PRIu64", %"PRIu64"), ", node->element, node->localIndex);
			node = node->nextNode;
		}
		printf("\n");
	}
	printf("Col element lists:\n");
	for (i = 0; i < colCount; ++i) {
		printf("Col %"PRIu64": ", i);
		node = colElementLists[i];
		while (node) {
			printf("(%"PRIu64", %"PRIu64"), ", node->element, node->localIndex);
			node = node->nextNode;
		}
		printf("\n");
	}
	printf("Elements:\n");
	for (i = 0; i < colCount; ++i) {
		if (!elements[i]) {
			printf("Element %"PRIu64" doesn't exist.\n", i);
			continue;
		}
		printf("Element %"PRIu64":\n", i);
		printf("L count live/all: %"PRIu64"/%"PRIu64"\n", elements[i]->liveLeCount, elements[i]->LeCount);
		printf("L: ");
		for (j = 0; j < elements[i]->LeCount; ++j) {
			printf("%"PRIu64, elements[i]->Le[j]);
			if (elements[i]->LeDeleted[j]) {
				printf(" (del)");
			}
			printf(", ");
		}
		printf("\n");
		printf("U count live/all: %"PRIu64"/%"PRIu64"\n", elements[i]->liveUeCount, elements[i]->UeCount);
		printf("U: ");
		for (j = 0; j < elements[i]->UeCount; ++j) {
			printf("%"PRIu64, elements[i]->Ue[j]);
			if (elements[i]->UeDeleted[j]) {
				printf(" (del)");
			}
			printf(", ");
		}
		printf("\n");
		printf("Contribution block:\n");
		for (mod = 0; mod < moduleCount; ++mod) {
			printf("For module %"PRIu64":\n", modules[mod]);
			for (j = 0; j < elements[i]->LeCount; ++j) {
				for (k = 0; k < elements[i]->UeCount; ++k) {
					printf("%010"PRIu64", ", elements[i]->contributionBlock[mod][j * elements[i]->UeCount + k]);
				}
				printf("\n");
			}
			printf("\n");
		}
	}
	printf("\n");
}

double timeMillis(struct timeval in) {
	double millis = 0;

	millis = in.tv_sec * 1000;
	millis += (double) in.tv_usec / 1000;
	return millis;
}

void printLUTime(SparseLUTime time) {
	printf("Measured wall clock time of LU factorization:\n");

	printf("Total: %.3f ms\n", timeMillis(time.total.elapsed));

	printf("Outer:\n");
	printf("\tAlgorithm initialization and allocations: %.3f ms\n", timeMillis(time.Outer.init.elapsed));
	printf("\tPre-assembly and allocation of frontal matrices: %.3f ms\n", timeMillis(time.Outer.frontalPreAssembly.elapsed));
	printf("\tRuns of the frontal matrix assembly loop: %.3f ms\n", timeMillis(time.Outer.frontalLoop.elapsed));
	printf("\tMumerical factorizations of frontal matrices: %.3f ms\n", timeMillis(time.Outer.frontalFactor.elapsed));
	printf("\tAlgorithm finalization and deallocation: %.3f ms\n", timeMillis(time.Outer.finalize.elapsed));

	printf("Frontal matrix assembly loop details:\n");
	printf("\tPlacement of pivot row and col into frontal matrices: %.3f ms\n", timeMillis(time.FrontalLoop.pivotPlacement.elapsed));
	printf("\tCalculation of external row and col degrees of elements: %.3f ms\n", timeMillis(time.FrontalLoop.extDegreeCalc.elapsed));
	printf("\tAssembling of other rows and cols from original matrix and contribution blocks into frontal matrices: %.3f ms\n", timeMillis(time.FrontalLoop.otherAssembly.elapsed));
	printf("\tFinding next local pivot candidates in frontal matrices: %.3f ms\n", timeMillis(time.FrontalLoop.findLocal.elapsed));
	printf("\tPre-assembly of next local pivot candidates: %.3f ms\n", timeMillis(time.FrontalLoop.assembleLocal.elapsed));
}

void printSolveTime(SolveLUTime time) {
	printf("Measured wall clock time of LU system solution:\n");

	printf("Total: %.3f ms\n", timeMillis(time.total.elapsed));
	printf("Detail:\n");
	printf("\tRow permutation: %.3f ms\n", timeMillis(time.rowPerm.elapsed));
	printf("\tLower triangular solve: %.3f ms\n", timeMillis(time.lower.elapsed));
	printf("\tUpper triangular solve: %.3f ms\n", timeMillis(time.upper.elapsed));
	printf("\tCol permutation: %.3f ms\n", timeMillis(time.colPerm.elapsed));
}
