/*
 * FactorFrontal.c
 *
 *  Created on: Feb 24, 2017
 *      Author: plech
 */

#include "types.h"
#include <stdio.h>
#include <sys/time.h>

#ifdef __cplusplus
extern "C" {
#include "FactorFrontal.h"
#include "Utils.h"
}
#endif

#define HANDLE_ERROR( err ) (HandleError( err, __FILE__, __LINE__ ))

static void HandleError(cudaError_t err, const char *file, int line) {
	if (err != cudaSuccess) {
		printf("%s in %s at line %d\n", cudaGetErrorString(err), file, line);
		exit( EXIT_FAILURE);
	}
}
__shared__ SharedMemory shared;

#include "FactorPivotBlock.fragment.cu"
#include "UpdatePivotRowsAndCols.fragment.cu"
#include "UpdateContributionBlock.fragment.cu"

__global__ void factorFrontalMatrixDev(TaskDescriptor *taskQueue, int taskCount) {
	int result;

	if (threadIdx.x == 0) {
		shared.task = taskQueue[blockIdx.x];
		*(shared.task.result) = 0;
	}
	__syncthreads(); // make task available to all threads
	result = factorPivotBlock();
	if (result) {
		if (threadIdx.x == 0) *(shared.task.result) = result;
		return;
	}
	// don't sync, pivot block is complete in shared memory where it's needed
	updatePivotRowsAndCols();
	__syncthreads(); // need pivot rows and cols complete in global memory for updating contribution block
	updateContributionBlock();
}

int factorFrontalMatrix(TaskDescriptor *taskQueue, int taskCount) {
	int dev, i;
	cudaDeviceProp prop;
	cudaEvent_t start, stop;
	float time;
	size_t totalFree, totalMemory;
	int result, minResult;
#ifdef DEBUG
	struct timeval t1, t2;
	double elapsedTime;
#endif
	TaskDescriptor *taskQueueHost, *taskQueueDev;

	cudaEventCreate(&start);
	cudaEventCreate(&stop);

	HANDLE_ERROR(cudaGetDevice(&dev));
	HANDLE_ERROR(cudaGetDeviceProperties(&prop, dev));
	HANDLE_ERROR(cudaMemGetInfo(&totalFree, &totalMemory));

#ifdef DEBUG
	printf("Device: %s\n", prop.name);
	printf("Free memory: %lu / %lu MB\n", totalFree / 1024 / 1024, totalMemory / 1024 / 1024);
	gettimeofday(&t1, 0);
#endif

	taskQueueHost = (TaskDescriptor *)safe_malloc(taskCount, sizeof(TaskDescriptor));
	for (i = 0; i < taskCount; ++i) {
		taskQueueHost[i].module = taskQueue[i].module;
		taskQueueHost[i].contributionBlockRows = taskQueue[i].contributionBlockRows;
		taskQueueHost[i].contributionBlockCols = taskQueue[i].contributionBlockCols;
		taskQueueHost[i].pivotCount = taskQueue[i].pivotCount;

		HANDLE_ERROR(cudaMalloc((void **) &(taskQueueHost[i].pivotBlock), taskQueue[i].pivotCount * taskQueue[i].pivotCount * sizeof(Value)));
		HANDLE_ERROR(cudaMalloc((void **) &(taskQueueHost[i].pivotRowBlock), taskQueue[i].pivotCount * taskQueue[i].contributionBlockCols * sizeof(Value)));
		HANDLE_ERROR(cudaMalloc((void **) &(taskQueueHost[i].pivotColBlock), taskQueue[i].contributionBlockRows * taskQueue[i].pivotCount * sizeof(Value)));
		HANDLE_ERROR(cudaMalloc((void **) &(taskQueueHost[i].contributionBlock), taskQueue[i].contributionBlockRows * taskQueue[i].contributionBlockCols * sizeof(Value)));
		HANDLE_ERROR(cudaMalloc((void **) &(taskQueueHost[i].result), sizeof(int)));

		HANDLE_ERROR(cudaMemcpy(taskQueueHost[i].pivotBlock, taskQueue[i].pivotBlock, taskQueue[i].pivotCount * taskQueue[i].pivotCount * sizeof(Value), cudaMemcpyHostToDevice));
		HANDLE_ERROR(cudaMemcpy(taskQueueHost[i].pivotRowBlock, taskQueue[i].pivotRowBlock, taskQueue[i].pivotCount * taskQueue[i].contributionBlockCols * sizeof(Value), cudaMemcpyHostToDevice));
		HANDLE_ERROR(cudaMemcpy(taskQueueHost[i].pivotColBlock, taskQueue[i].pivotColBlock, taskQueue[i].contributionBlockRows * taskQueue[i].pivotCount * sizeof(Value), cudaMemcpyHostToDevice));
		HANDLE_ERROR(cudaMemcpy(taskQueueHost[i].contributionBlock, taskQueue[i].contributionBlock, taskQueue[i].contributionBlockRows * taskQueue[i].contributionBlockCols * sizeof(Value), cudaMemcpyHostToDevice));
	}
	HANDLE_ERROR(cudaMalloc((void **) &(taskQueueDev), taskCount * sizeof(TaskDescriptor)));
	HANDLE_ERROR(cudaMemcpy(taskQueueDev, taskQueueHost, taskCount * sizeof(TaskDescriptor), cudaMemcpyHostToDevice));

	cudaEventRecord(start, 0);
	factorFrontalMatrixDev<<<taskCount, NUMTHREADS>>>(taskQueueDev, taskCount);
	cudaEventRecord(stop, 0);
	cudaEventSynchronize(stop);
	cudaEventElapsedTime(&time, start, stop);

	minResult = PIVOT_BLOCK_SIZE;
	for (i = 0; i < taskCount; ++i) {
		HANDLE_ERROR(cudaMemcpy(&result, taskQueueHost[i].result, sizeof(int), cudaMemcpyDeviceToHost));
#ifdef DEBUG
		printf("Task %d ended with result %d.\n", i, result);
#endif
		if (result && result < minResult) minResult = result;
	}

	result = minResult < PIVOT_BLOCK_SIZE ? minResult : 0;
#ifdef DEBUG
	printf("Total result is %d.\n", result);
#endif
	if (!result) {
		for (i = 0; i < taskCount; ++i) {
			HANDLE_ERROR(cudaMemcpy(taskQueue[i].pivotBlock, taskQueueHost[i].pivotBlock, taskQueue[i].pivotCount * taskQueue[i].pivotCount * sizeof(Value), cudaMemcpyDeviceToHost));
			HANDLE_ERROR(cudaMemcpy(taskQueue[i].pivotRowBlock, taskQueueHost[i].pivotRowBlock, taskQueue[i].pivotCount * taskQueue[i].contributionBlockCols * sizeof(Value), cudaMemcpyDeviceToHost));
			HANDLE_ERROR(cudaMemcpy(taskQueue[i].pivotColBlock, taskQueueHost[i].pivotColBlock, taskQueue[i].contributionBlockRows * taskQueue[i].pivotCount * sizeof(Value), cudaMemcpyDeviceToHost));
			HANDLE_ERROR(cudaMemcpy(taskQueue[i].contributionBlock, taskQueueHost[i].contributionBlock, taskQueue[i].contributionBlockRows * taskQueue[i].contributionBlockCols * sizeof(Value), cudaMemcpyDeviceToHost));
		}
	}

	for (i = 0; i < taskCount; ++i) {
		HANDLE_ERROR(cudaFree(taskQueueHost[i].pivotBlock));
		HANDLE_ERROR(cudaFree(taskQueueHost[i].pivotRowBlock));
		HANDLE_ERROR(cudaFree(taskQueueHost[i].pivotColBlock));
		HANDLE_ERROR(cudaFree(taskQueueHost[i].contributionBlock));
		HANDLE_ERROR(cudaFree(taskQueueHost[i].result));
	}
	HANDLE_ERROR(cudaFree(taskQueueDev));
	taskQueueHost = (TaskDescriptor *)safe_free(taskQueueHost);

#ifdef DEBUG
	gettimeofday(&t2, 0);
	printf ("Time for the kernel: %.3f ms\n", time);
	elapsedTime = (t2.tv_sec - t1.tv_sec) * 1000;
	elapsedTime += ((double) t2.tv_usec - (double) t1.tv_usec) / 1000;
	printf("Total time including transfer: %.3f ms.\n", elapsedTime);
#endif

	return result;
}
