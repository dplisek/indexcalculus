/*
 * ElementLists.c
 *
 *  Created on: Mar 15, 2017
 *      Author: plech
 */

#include <stdlib.h>
#include "types.h"
#include "ElementLists.h"
#include "Utils.h"

void traverseElementList(ElementListNode **startNode, Element **elements, void (*work)(ElementListNode *, Element **, void *), void *data, int colwise) {
	ElementListNode *previousNode, *node, *nextNode;
	Element *element;

	node = *startNode;
	previousNode = 0;
	while (node) {
		nextNode = node->nextNode;
		element = elements[node->element];
		if (!element
				|| (!colwise && element->LeDeleted[node->localIndex])
				|| (colwise && element->UeDeleted[node->localIndex])) {
			if (previousNode) {
				previousNode->nextNode = nextNode;
			} else {
				*startNode = nextNode;
			}
			node = safe_free(node);
		} else {
			work(node, elements, data);
			previousNode = node;
		}
		node = nextNode;
	}
}

typedef struct update_w_col_t {

	MatrixIndex *wCol, wZeroShift, *wMax;

} UpdateWColData;

void updateWCol(ElementListNode *node, Element **elements, void *d) {
	UpdateWColData *data = (UpdateWColData *)d;
	if (data->wCol[node->element] < data->wZeroShift) {
		data->wCol[node->element] = data->wZeroShift + elements[node->element]->liveLeCount;
		if (data->wCol[node->element] - 1 > *(data->wMax)) *(data->wMax) = data->wCol[node->element] - 1;
	}
	--(data->wCol[node->element]);
}

void calculateExternalColDegrees(MatrixIndex *L, MatrixIndex oldLcount, MatrixIndex Lcount, ElementListNode **rowElementLists, Element **elements,
		MatrixIndex *wCol, MatrixIndex wZeroShift, MatrixIndex *wMax) {

	MatrixIndex i;
	UpdateWColData data;

	data.wCol = wCol;
	data.wMax = wMax;
	data.wZeroShift = wZeroShift;

	for (i = oldLcount; i < Lcount; ++i) {
		traverseElementList(rowElementLists + L[i], elements, &updateWCol, &data, 0);
	}
}

typedef struct update_w_row_t {

	MatrixIndex *wRow, wZeroShift, *wMax;

} UpdateWRowData;

void updateWRow(ElementListNode *node, Element **elements, void *d) {
	UpdateWRowData *data = (UpdateWRowData *)d;
	if (data->wRow[node->element] < data->wZeroShift) {
		data->wRow[node->element] = data->wZeroShift + elements[node->element]->liveUeCount;
		if (data->wRow[node->element] - 1 > *(data->wMax)) *(data->wMax) = data->wRow[node->element] - 1;
	}
	--(data->wRow[node->element]);
}

void calculateExternalRowDegrees(MatrixIndex *U, MatrixIndex oldUcount, MatrixIndex Ucount, ElementListNode **colElementLists, Element **elements,
		MatrixIndex *wRow, MatrixIndex wZeroShift, MatrixIndex *wMax) {

	MatrixIndex j;
	UpdateWRowData data;

	data.wRow = wRow;
	data.wMax = wMax;
	data.wZeroShift = wZeroShift;

	for (j = oldUcount; j < Ucount; ++j) {
		traverseElementList(colElementLists + U[j], elements, &updateWRow, &data, 1);
	}
}

void *freeElement(Element *element, int moduleCount) {
	if (element) {
		element->Le = safe_free(element->Le);
		element->Ue = safe_free(element->Ue);
		element->LeDeleted = safe_free(element->LeDeleted);
		element->UeDeleted = safe_free(element->UeDeleted);
		element->contributionBlock = safe_free_incl_sublevel((void **)(element->contributionBlock), moduleCount);
	}
	return safe_free(element);
}

void *freeElements(Element **elements, MatrixIndex elementCount, int moduleCount) {
	MatrixIndex i;

	if (elements) {
		for (i = 0; i < elementCount; ++i) {
			elements[i] = freeElement(elements[i], moduleCount);
		}
	}
	return safe_free(elements);
}

void *freeElementLists(ElementListNode **elementLists, MatrixIndex listCount) {
	MatrixIndex i;
	ElementListNode *thisNode, *nextNode;

	if (elementLists) {
		for (i = 0; i < listCount; ++i) {
			thisNode = elementLists[i];
			while (thisNode) {
				nextNode = thisNode->nextNode;
				thisNode = safe_free(thisNode);
				thisNode = nextNode;
			}
			elementLists[i] = 0;
		}
	}
	return safe_free(elementLists);
}

void markColDeleted(Element **elements, ElementListNode *node, int moduleCount) {
	elements[node->element]->UeDeleted[node->localIndex] = 1;
	--(elements[node->element]->liveUeCount);
	if (!(elements[node->element]->liveUeCount)) {
		elements[node->element] = freeElement(elements[node->element], moduleCount);
	}
}

void assembleElementCol(Element **elements, ElementListNode *node, MatrixIndex *L, MatrixIndex *Lcount, Value **Xj, Value ***F, MatrixIndex j, Bool appending, Bool deleting, Value *modules, int moduleCount) {
	MatrixIndex iElem, iFrontal;
	Element *element;
	int mod;

	element = elements[node->element];
	for (iElem = 0; iElem < element->LeCount; ++iElem) {
		if (element->LeDeleted[iElem]) continue;
		for (iFrontal = 0; iFrontal < *Lcount; ++iFrontal) {
			if (L[iFrontal] != element->Le[iElem]) continue;
			break;
		}
		if (appending && iFrontal == *Lcount) {
			L[iFrontal] = element->Le[iElem];
			++(*Lcount);
		}
		if (appending || iFrontal < *Lcount) {
			for (mod = 0; mod < moduleCount; ++mod) {
				if (Xj) Xj[mod][iFrontal] = (Xj[mod][iFrontal] + element->contributionBlock[mod][iElem * element->UeCount + node->localIndex]) % modules[mod];
				if (F) F[mod][iFrontal][j] = (F[mod][iFrontal][j] + element->contributionBlock[mod][iElem * element->UeCount + node->localIndex]) % modules[mod];
			}
		}
	}
	if (deleting) {
		markColDeleted(elements, node, moduleCount);
	}
}

typedef struct assemble_element_col_t {

	MatrixIndex *L, *Lcount;
	Value **Xj, *modules;
	Bool appending, deleting;
	int moduleCount;

} AssembleElementColData;

void assembleElementColData(ElementListNode *node, Element **elements, void *d) {
	AssembleElementColData *data = (AssembleElementColData *)d;
	assembleElementCol(elements, node, data->L, data->Lcount, data->Xj, 0, 0, data->appending, data->deleting, data->modules, data->moduleCount);
}

void assembleElementCols(Element **elements, ElementListNode **startNode, MatrixIndex *L, MatrixIndex *Lcount, Value **Xj, Bool appending, Bool deleting, Value *modules, int moduleCount) {
	AssembleElementColData data;
	data.L = L;
	data.Lcount = Lcount;
	data.Xj = Xj;
	data.appending = appending;
	data.deleting = deleting;
	data.moduleCount = moduleCount;
	data.modules = modules;
	traverseElementList(startNode, elements, &assembleElementColData, &data, 1);
}

void markRowDeleted(Element **elements, ElementListNode *node, int moduleCount) {
	elements[node->element]->LeDeleted[node->localIndex] = 1;
	--(elements[node->element]->liveLeCount);
	if (!(elements[node->element]->liveLeCount)) {
		elements[node->element] = freeElement(elements[node->element], moduleCount);
	}
}

void assembleElementRow(Element **elements, ElementListNode *node, MatrixIndex *U, MatrixIndex *Ucount, Value **Xi, Value ***F, MatrixIndex i, Bool appending, Bool deleting, Value *modules, int moduleCount, MatrixIndex pivCol, MatrixIndex *pivColLocal) {
	MatrixIndex jElem, jFrontal;
	Element *element;
	int mod;

	element = elements[node->element];
	for (jElem = 0; jElem < element->UeCount; ++jElem) {
		if (element->UeDeleted[jElem]) continue;
		for (jFrontal = 0; jFrontal < *Ucount; ++jFrontal) {
			if (U[jFrontal] != element->Ue[jElem]) continue;
			break;
		}
		if (appending && jFrontal == *Ucount) {
			U[jFrontal] = element->Ue[jElem];
			if (pivColLocal && element->Ue[jElem] == pivCol) *pivColLocal = *Ucount;
			++(*Ucount);
		}
		if (appending || jFrontal < *Ucount) {
			for (mod = 0; mod < moduleCount; ++mod) {
				if (Xi) Xi[mod][jFrontal] = (Xi[mod][jFrontal] + element->contributionBlock[mod][node->localIndex * element->UeCount + jElem]) % modules[mod];
				if (F) F[mod][i][jFrontal] = (F[mod][i][jFrontal] + element->contributionBlock[mod][node->localIndex * element->UeCount + jElem]) % modules[mod];
			}
		}
	}
	if (deleting) {
		markRowDeleted(elements, node, moduleCount);
	}
}

typedef struct assemble_element_row_t {

	MatrixIndex *U, *Ucount, pivCol, *pivColLocal;
	Value **Xi, *modules;
	Bool appending, deleting;
	int moduleCount;

} AssembleElementRowData;

void assembleElementRowData(ElementListNode *node, Element **elements, void *d) {
	AssembleElementRowData *data = (AssembleElementRowData *)d;
	assembleElementRow(elements, node, data->U, data->Ucount, data->Xi, 0, 0, data->appending, data->deleting, data->modules, data->moduleCount, data->pivCol, data->pivColLocal);
}

void assembleElementRows(Element **elements, ElementListNode **startNode, MatrixIndex *U, MatrixIndex *Ucount, Value **Xi, Bool appending, Bool deleting, Value *modules, int moduleCount, MatrixIndex pivCol, MatrixIndex *pivColLocal) {
	AssembleElementRowData data;
	data.U = U;
	data.Ucount = Ucount;
	data.Xi = Xi;
	data.appending = appending;
	data.deleting = deleting;
	data.moduleCount = moduleCount;
	data.modules = modules;
	data.pivCol = pivCol;
	data.pivColLocal = pivColLocal;
	traverseElementList(startNode, elements, &assembleElementRowData, &data, 0);
}
