/*
 * FactorPivotBlock.cu
 *
 *  Created on: Feb 24, 2017
 *      Author: plech
 */

#include "types.h"
#include "FactorFrontal.h"
#include <stdio.h>

#define MY_BITTY_ROW			(threadIdx.x / PIVOT_BLOCK_SIZE)
#define MY_BITTY_COL			(threadIdx.x % PIVOT_BLOCK_SIZE)
#define THREAD_HANDLING_DIAG(k)	(PIVOT_BLOCK_SIZE * (k) + (k))
#define LAST_THREAD_IN_ROW(r)	(PIVOT_BLOCK_SIZE * (r) + shared.task.pivotCount - 1)

__inline__ __device__ void loadBittyBlockFromGlobalToRegister(Value *bittyBlock) {
	int row = MY_BITTY_ROW;
	int col = MY_BITTY_COL;
	int pivotCount = shared.task.pivotCount;
	if (row >= pivotCount || (col >= pivotCount)) return;
	*bittyBlock = shared.task.pivotBlock[row * pivotCount + col];
}

__inline__ __device__ void calculateAndStorePivotInverse(Value bittyBlock, int step) {
	Value u, v, x1, x2, r, x, q, module;

	if (threadIdx.x != THREAD_HANDLING_DIAG(step)) return;

	module = shared.task.module;
	if (!bittyBlock) {
		shared.Data.UpdatePivots.invertedPivots[step] = module;
		return;
	}

	// Extended Euclid
	// u and v start at <= module and are decremented throughout, staying positive, no need for moduling
	// x1 and x2 jump all around the field, need to module them

	u = bittyBlock;
	v = module;
	x1 = 1;
	x2 = 0;
	while (u != 1) {
		q = v / u;
		r = v - q * u;
		x = (q * x1) % module;
		x = (module + x2 - x) % module; // prepend module so that subtraction doesn't go negative
		v = u;
		u = r;
		x2 = x1;
		x1 = x;
	}
	shared.Data.UpdatePivots.invertedPivots[step] = x1;
}

__inline__ __device__ void storePivotRow(Value bittyBlock, int step) {
	if (threadIdx.x < THREAD_HANDLING_DIAG(step) || threadIdx.x > LAST_THREAD_IN_ROW(step)) return;
	shared.Data.UpdatePivots.pivotBlock[step][MY_BITTY_COL] = bittyBlock;
}

__inline__ __device__ void updateAndStorePivotCol(Value *bittyBlock, int step) {
	int row;

	if (MY_BITTY_COL != step) return;

	row = MY_BITTY_ROW;
	if (row <= step || row >= shared.task.pivotCount) return;
	*bittyBlock = (*bittyBlock * shared.Data.UpdatePivots.invertedPivots[step]) % shared.task.module;
	shared.Data.UpdatePivots.pivotBlock[row][step] = *bittyBlock;
}

__inline__ __device__ void updateRemainingSubmatrix(Value *bittyBlock, int step) {
	int row = MY_BITTY_ROW;
	int col = MY_BITTY_COL;
	int pivotCount = shared.task.pivotCount;
	Value module;

	if (row <= step || row >= pivotCount) return;
	if (col <= step || col >= pivotCount) return;

	module = shared.task.module;
	*bittyBlock = (module + *bittyBlock - ((shared.Data.UpdatePivots.pivotBlock[step][col] * shared.Data.UpdatePivots.pivotBlock[row][step]) % module)) % module; // prepend module before subtracting
}

__inline__ __device__ int performFactorLoop(Value *bittyBlock) {
	int step;

	#pragma unroll
	for (step = 0; step < shared.task.pivotCount; ++step) {
		calculateAndStorePivotInverse(*bittyBlock, step);
		__syncthreads(); // update pivot col action needs inverse stored
		if (step && shared.Data.UpdatePivots.invertedPivots[step] == shared.task.module) {
			return step;
		}
		storePivotRow(*bittyBlock, step);
		updateAndStorePivotCol(bittyBlock, step);
		__syncthreads(); // need stored pivot row and col for next step
		updateRemainingSubmatrix(bittyBlock, step);
	}
	return 0;
}

__inline__ __device__ void writeBittyBlockFromRegisterToGlobal(Value bittyBlock) {
	int row = MY_BITTY_ROW;
	int col = MY_BITTY_COL;
	int pivotCount = shared.task.pivotCount;
	if (row >= pivotCount || (col >= pivotCount)) return;
	shared.task.pivotBlock[row * pivotCount + col] = bittyBlock;
}

__device__ int factorPivotBlock() {
	Value bittyBlock;
	int result;

	loadBittyBlockFromGlobalToRegister(&bittyBlock);
	result = performFactorLoop(&bittyBlock);
	if (result) return result;
	writeBittyBlockFromRegisterToGlobal(bittyBlock);
	return 0;
}

#undef MY_BITTY_ROW
#undef MY_BITTY_COL
