#include "types.h"

#define BITTY_BLOCK_COUNT			3
#define BITTY_BLOCK_SIZE			4
#define THREADS_IN_COL				4
#define ROWS_BITTY_ROW(i)			(threadIdx.x % THREADS_IN_COL + (i) * THREADS_IN_COL)
#define ROWS_BITTY_COL				(threadIdx.x / THREADS_IN_COL)
#define COLS_BITTY_ROW				((threadIdx.x - NUMTHREADS / 2) / THREADS_IN_COL)
#define COLS_BITTY_COL(i)			ROWS_BITTY_ROW(i)
#define HANDLING_PIVOT_ROWS			(threadIdx.x < NUMTHREADS / 2)
#define MAIN_THREAD_IN_STEP(step)	(threadIdx.x % THREADS_IN_COL == (step) % THREADS_IN_COL)
#define INDEX_IN_BITTY_BLOCK(step)	((step) / THREADS_IN_COL)

typedef enum {BittyBlockSendDirectionRegToGlobal, BittyBlockSendDirectionGlobalToReg} BittyBlockSendDirection;

__inline__ __device__ void sendRowBittyBlock(Value bittyBlock[], MatrixIndex segment, BittyBlockSendDirection direction) {
	int i;
	MatrixIndex row, col, globalCol;

	col = ROWS_BITTY_COL;
	globalCol = segment * PIVOT_ROW_COL_SEGMENT_SIZE + col;
	#pragma unroll
	for (i = 0; i < BITTY_BLOCK_SIZE; ++i) {
		row = ROWS_BITTY_ROW(i);
		if (row >= shared.task.pivotCount) break;
		if (direction == BittyBlockSendDirectionGlobalToReg) {
			if (globalCol < shared.task.contributionBlockCols) {
				bittyBlock[i] = shared.task.pivotRowBlock[row * shared.task.contributionBlockCols + globalCol];
			} else {
				bittyBlock[i] = 0;
			}
		} else if (globalCol < shared.task.contributionBlockCols) {
			shared.task.pivotRowBlock[row * shared.task.contributionBlockCols + globalCol] = bittyBlock[i];
		}
	}
}

__inline__ __device__ void sendColBittyBlock(Value bittyBlock[], MatrixIndex segment, BittyBlockSendDirection direction) {
	int i;
	MatrixIndex row, col, globalRow;
	int pivotCount = shared.task.pivotCount;

	row = COLS_BITTY_ROW;
	globalRow = segment * PIVOT_ROW_COL_SEGMENT_SIZE + row;
	#pragma unroll
	for (i = 0; i < BITTY_BLOCK_SIZE; ++i) {
		col = COLS_BITTY_COL(i);
		if (col >= pivotCount) break;
		if (direction == BittyBlockSendDirectionGlobalToReg) {
			if (globalRow < shared.task.contributionBlockRows) {
				bittyBlock[i] = shared.task.pivotColBlock[globalRow * pivotCount + col];
			} else {
				bittyBlock[i] = 0;
			}
		} else if (globalRow < shared.task.contributionBlockRows) {
			shared.task.pivotColBlock[globalRow * pivotCount + col] = bittyBlock[i];
		}
	}
}

__inline__ __device__ void sendBittyBlock(Value bittyBlock[], MatrixIndex segment, BittyBlockSendDirection direction) {
	if (HANDLING_PIVOT_ROWS) {
		sendRowBittyBlock(bittyBlock, segment, direction);
	} else {
		sendColBittyBlock(bittyBlock, segment, direction);
	}
}

__inline__ __device__ void getInvertedPivots(Value invertedPivots[]) {
	int i;
	MatrixIndex col;
	int pivotCount = shared.task.pivotCount;

	#pragma unroll
	for (i = 0; i < BITTY_BLOCK_SIZE; ++i) {
		col = COLS_BITTY_COL(i);
		if (col >= pivotCount) break;
		invertedPivots[i] = shared.Data.UpdatePivots.invertedPivots[col];
	}
}

__inline__ __device__ void computeSegment(Value bittyBlock[], Value invertedPivots[]) {
	int step, indexInBittyBlock, i;
	MatrixIndex row, col;
	Value module, updater;

	module = shared.task.module;

	#pragma unroll
	for (step = 0; step < shared.task.pivotCount; ++step) {
		indexInBittyBlock = INDEX_IN_BITTY_BLOCK(step);
		if (MAIN_THREAD_IN_STEP(step)) {
			if (HANDLING_PIVOT_ROWS) {
				shared.Data.UpdatePivots.rowsSegment[step][ROWS_BITTY_COL] = bittyBlock[indexInBittyBlock];
			} else {
				bittyBlock[indexInBittyBlock] = bittyBlock[indexInBittyBlock] * invertedPivots[indexInBittyBlock] % module;
				shared.Data.UpdatePivots.colsSegment[COLS_BITTY_ROW][step] = bittyBlock[indexInBittyBlock];
			}
		}
		__syncthreads(); // need shared memory write finished
		#pragma unroll
		for (i = INDEX_IN_BITTY_BLOCK(step + 1); i < BITTY_BLOCK_SIZE; ++i) {
			if (HANDLING_PIVOT_ROWS) {
				row = ROWS_BITTY_ROW(i);
				if (row <= step) continue;
				if (row >= shared.task.pivotCount) break;
				updater = shared.Data.UpdatePivots.pivotBlock[row][step] * shared.Data.UpdatePivots.rowsSegment[step][ROWS_BITTY_COL] % module;
			} else {
				col = COLS_BITTY_COL(i);
				if (col <= step) continue;
				if (col >= shared.task.pivotCount) break;
				updater = shared.Data.UpdatePivots.pivotBlock[step][col] * shared.Data.UpdatePivots.colsSegment[COLS_BITTY_ROW][step] % module;
			}
			bittyBlock[i] = (module + bittyBlock[i] - updater) % module;
		}
	}
}

__device__ void updatePivotRowsAndCols() {
	int bittyBlockIndex;
	MatrixIndex segment;
	Value bittyBlocks[BITTY_BLOCK_COUNT][BITTY_BLOCK_SIZE];
	Value invertedPivots[BITTY_BLOCK_SIZE];

//	if (shared.task.pivotRowColBlockSize <= 0) return; // can't leave here, frontal matrix can be rectangular and subsequent syncs would become conditioned - undefined behavior

	if (!HANDLING_PIVOT_ROWS) {
		getInvertedPivots(invertedPivots);
	}

	sendBittyBlock(bittyBlocks[0], 0, BittyBlockSendDirectionGlobalToReg);
	// __syncthreads(); should not be needed in this outer loop, because each thread loads its own data to work on, doesn't go through shared
	for (segment = 0, bittyBlockIndex = 0; (segment + 1) * PIVOT_ROW_COL_SEGMENT_SIZE < shared.task.contributionBlockCols || (segment + 1) * PIVOT_ROW_COL_SEGMENT_SIZE < shared.task.contributionBlockRows; ++segment, bittyBlockIndex = (bittyBlockIndex + 1) % BITTY_BLOCK_COUNT) {
		sendBittyBlock(bittyBlocks[(bittyBlockIndex + 1) % BITTY_BLOCK_COUNT], segment + 1, BittyBlockSendDirectionGlobalToReg);
		computeSegment(bittyBlocks[bittyBlockIndex], invertedPivots);
		__syncthreads(); // don't overlap computeSegment calls, they use the same shared memory segment buffer
		sendBittyBlock(bittyBlocks[bittyBlockIndex], segment, BittyBlockSendDirectionRegToGlobal);
	}
	computeSegment(bittyBlocks[bittyBlockIndex], invertedPivots);
	sendBittyBlock(bittyBlocks[bittyBlockIndex], segment, BittyBlockSendDirectionRegToGlobal);
}

#undef BITTY_BLOCK_SIZE
#undef THREADS_IN_COL
