/*
 * UpdateContributionBlock.fragment.cu
 *
 *  Created on: Mar 3, 2017
 *      Author: plech
 */

#include <inttypes.h>

#define TRANSFER_BITTY_BLOCK_SIZE			1
#define PIV_ROW_TRANSFER_ALL_THREAD_ROWS	12
#define PIV_ROW_TRANSFER_MY_BITTY_ROW(i)	(threadIdx.x / CONTRIBUTION_BLOCK_SEG_COLS + (i) * PIV_ROW_TRANSFER_ALL_THREAD_ROWS)
#define PIV_ROW_TRANSFER_MY_BITTY_COL		(threadIdx.x % CONTRIBUTION_BLOCK_SEG_COLS)
#define PIV_COL_TRANSFER_ALL_THREAD_ROWS	24
#define PIV_COL_TRANSFER_MY_BITTY_ROW(i)	(threadIdx.x / PIVOT_BLOCK_SIZE + (i) * PIV_COL_TRANSFER_ALL_THREAD_ROWS)
#define PIV_COL_TRANSFER_MY_BITTY_COL		(threadIdx.x % PIVOT_BLOCK_SIZE)

#define BITTY_BLOCK_SIZE					2
#define THREADS_IN_COL						32
#define MY_BITTY_ROW(i)						(threadIdx.x / CONTRIBUTION_BLOCK_SEG_COLS + (i) * THREADS_IN_COL)
#define MY_BITTY_COL						(threadIdx.x % CONTRIBUTION_BLOCK_SEG_COLS)

__inline__ __device__ void loadPivotRowsTransferBittyBlockFromGlobal(Value transferBittyBlock[], MatrixIndex segment) {
	int i;
	MatrixIndex row, col, globalCol;

	col = PIV_ROW_TRANSFER_MY_BITTY_COL;
	globalCol = segment * CONTRIBUTION_BLOCK_SEG_COLS + col;
	#pragma unroll
	for (i = 0; i < TRANSFER_BITTY_BLOCK_SIZE; ++i) {
		row = PIV_ROW_TRANSFER_MY_BITTY_ROW(i);
		if (row >= shared.task.pivotCount) break;
		if (globalCol < shared.task.contributionBlockCols) {
			transferBittyBlock[i] = shared.task.pivotRowBlock[row * shared.task.contributionBlockCols + globalCol];
		} else {
			transferBittyBlock[i] = 0;
		}
	}
}

__inline__ __device__ void loadPivotColsTransferBittyBlockFromGlobal(Value transferBittyBlock[], MatrixIndex segment) {
	int i;
	MatrixIndex row, col, globalRow;

	col = PIV_COL_TRANSFER_MY_BITTY_COL;
	if (col >= shared.task.pivotCount) return;
	#pragma unroll
	for (i = 0; i < TRANSFER_BITTY_BLOCK_SIZE; ++i) {
		row = PIV_COL_TRANSFER_MY_BITTY_ROW(i);
		if (row >= CONTRIBUTION_BLOCK_SEG_ROWS) break;
		globalRow = segment * CONTRIBUTION_BLOCK_SEG_ROWS + row;
		if (globalRow < shared.task.contributionBlockRows) {
			transferBittyBlock[i] = shared.task.pivotColBlock[globalRow * shared.task.pivotCount + col];
		} else {
			transferBittyBlock[i] = 0;
		}
	}
}

__inline__ __device__ void savePivotRowsTransferBittyBlockToShared(Value transferBittyBlock[], int alteration) {
	int i;
	MatrixIndex row, col;

	col = PIV_ROW_TRANSFER_MY_BITTY_COL;
	#pragma unroll
	for (i = 0; i < TRANSFER_BITTY_BLOCK_SIZE; ++i) {
		row = PIV_ROW_TRANSFER_MY_BITTY_ROW(i);
		if (row >= shared.task.pivotCount) break;
		shared.Data.UpdateContributionBlock.rowsSegments[alteration][row][col] = transferBittyBlock[i];
	}
}

__inline__ __device__ void savePivotColsTransferBittyBlockToShared(Value transferBittyBlock[], int alteration) {
	int i;
	MatrixIndex row, col;

	col = PIV_COL_TRANSFER_MY_BITTY_COL;
	if (col >= shared.task.pivotCount) return;
	#pragma unroll
	for (i = 0; i < TRANSFER_BITTY_BLOCK_SIZE; ++i) {
		row = PIV_COL_TRANSFER_MY_BITTY_ROW(i);
		if (row >= CONTRIBUTION_BLOCK_SEG_ROWS) break;
		shared.Data.UpdateContributionBlock.colsSegments[alteration][row][col] = transferBittyBlock[i];
	}
}

__inline__ __device__ void getNextSegment(MatrixIndex *pivotRowsSegment, MatrixIndex *pivotColsSegment, bool *segmentChanged) {
	if (shared.task.contributionBlockCols < shared.task.contributionBlockRows) {
		if ((*pivotColsSegment + 1) * CONTRIBUTION_BLOCK_SEG_ROWS < shared.task.contributionBlockRows) {
			(*pivotColsSegment)++;
			*segmentChanged = true;
		} else {
			if ((*pivotRowsSegment + 1) * CONTRIBUTION_BLOCK_SEG_COLS < shared.task.contributionBlockCols) {
				*pivotColsSegment = 0;
				(*pivotRowsSegment)++;
				*segmentChanged = true;
			} else {
				*segmentChanged = false;
			}
		}
	} else {
		if ((*pivotRowsSegment + 1) * CONTRIBUTION_BLOCK_SEG_COLS < shared.task.contributionBlockCols) {
			(*pivotRowsSegment)++;
			*segmentChanged = true;
		} else {
			if ((*pivotColsSegment + 1) * CONTRIBUTION_BLOCK_SEG_ROWS < shared.task.contributionBlockRows) {
				*pivotRowsSegment = 0;
				(*pivotColsSegment)++;
				*segmentChanged = true;
			} else {
				*segmentChanged = false;
			}
		}
	}
}

__inline__ __device__ void calculateDecrement(Value bittyBlock[], int alteration) {
	int i, pivot;
	Value pivotRowValue, temp, module = shared.task.module;

	for (i = 0; i < BITTY_BLOCK_SIZE; ++i) {
		bittyBlock[i] = 0;
	}
	for (pivot = 0; pivot < shared.task.pivotCount; ++pivot) {
		pivotRowValue = shared.Data.UpdateContributionBlock.rowsSegments[alteration][pivot][MY_BITTY_COL];
		for (i = 0; i < BITTY_BLOCK_SIZE; ++i) {
			temp = (pivotRowValue * shared.Data.UpdateContributionBlock.colsSegments[alteration][MY_BITTY_ROW(i)][pivot]) % module;
			bittyBlock[i] = (bittyBlock[i] + temp) % module;
		}
	}
}

__inline__ __device__ void decrementGlobal(int pivotRowsSegment, int pivotColsSegment, Value bittyBlock[]) {
	int i;
	MatrixIndex globalRow, globalCol;
	Value module = shared.task.module;

	globalCol = pivotRowsSegment * CONTRIBUTION_BLOCK_SEG_COLS + MY_BITTY_COL;
	if (globalCol >= shared.task.contributionBlockCols) return;
	for (i = 0; i < BITTY_BLOCK_SIZE; ++i) {
		globalRow = pivotColsSegment * CONTRIBUTION_BLOCK_SEG_ROWS + MY_BITTY_ROW(i);
		if (globalRow >= shared.task.contributionBlockRows) break;
		shared.task.contributionBlock[globalRow * shared.task.contributionBlockCols + globalCol] = (module + shared.task.contributionBlock[globalRow * shared.task.contributionBlockCols + globalCol] - bittyBlock[i]) % module;
	}
}

__device__ void updateContributionBlock() {
	MatrixIndex pivotRowsSegment, pivotColsSegment, pivotRowsSegmentsSavedInShared[CONTRIBUTION_BLOCK_ALTERATIONS], pivotColsSegmentsSavedInShared[CONTRIBUTION_BLOCK_ALTERATIONS];
	int alteration, i;
	bool segmentChanged;
	Value pivotRowsTransferBittyBlock[TRANSFER_BITTY_BLOCK_SIZE];
	Value pivotColsTransferBittyBlock[TRANSFER_BITTY_BLOCK_SIZE];
	Value bittyBlock[BITTY_BLOCK_SIZE];

	alteration = 0;
	pivotRowsSegment = pivotColsSegment = 0;
	for (i = 0; i < CONTRIBUTION_BLOCK_ALTERATIONS; ++i) {
		pivotRowsSegmentsSavedInShared[i] = UINT64_MAX;
		pivotColsSegmentsSavedInShared[i] = UINT64_MAX;
	}
	segmentChanged = true;

	loadPivotRowsTransferBittyBlockFromGlobal(pivotRowsTransferBittyBlock, pivotRowsSegment);
	loadPivotColsTransferBittyBlockFromGlobal(pivotColsTransferBittyBlock, pivotColsSegment);

	while (segmentChanged) {
		if (pivotRowsSegmentsSavedInShared[alteration] != pivotRowsSegment) {
			savePivotRowsTransferBittyBlockToShared(pivotRowsTransferBittyBlock, alteration);
			pivotRowsSegmentsSavedInShared[alteration] = pivotRowsSegment;
		}
		if (pivotColsSegmentsSavedInShared[alteration] != pivotColsSegment) {
			savePivotColsTransferBittyBlockToShared(pivotColsTransferBittyBlock, alteration);
			pivotColsSegmentsSavedInShared[alteration] = pivotColsSegment;
		}
		__syncthreads();
		getNextSegment(&pivotRowsSegment, &pivotColsSegment, &segmentChanged);
		if (segmentChanged && pivotRowsSegmentsSavedInShared[(alteration + 1) % CONTRIBUTION_BLOCK_ALTERATIONS] != pivotRowsSegment) {
			loadPivotRowsTransferBittyBlockFromGlobal(pivotRowsTransferBittyBlock, pivotRowsSegment);
		}
		if (segmentChanged && pivotColsSegmentsSavedInShared[(alteration + 1) % CONTRIBUTION_BLOCK_ALTERATIONS] != pivotColsSegment) {
			loadPivotColsTransferBittyBlockFromGlobal(pivotColsTransferBittyBlock, pivotColsSegment);
		}
		calculateDecrement(bittyBlock, alteration);
		decrementGlobal(pivotRowsSegmentsSavedInShared[alteration], pivotColsSegmentsSavedInShared[alteration], bittyBlock);
		alteration = (alteration + 1) % CONTRIBUTION_BLOCK_ALTERATIONS;
	}
}

#undef BITTY_BLOCK_SIZE
