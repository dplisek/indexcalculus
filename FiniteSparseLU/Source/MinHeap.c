/*
 * MinHeap.c
 *
 *  Created on: Mar 17, 2017
 *      Author: plech
 */

#include <stdlib.h>
#include "MinHeap.h"

#define LCHILD(x)	(2 * (x) + 1)
#define RCHILD(x) 	(2 * (x) + 2)
#define PARENT(x) 	(((x) - 1) / 2)

void heapifyColCandidatesDown(MatrixIndex *colHeap, MatrixIndex *colHeapInv, MatrixIndex size, MatrixIndex index, MatrixIndex *dc) {
	MatrixIndex temp;
    MatrixIndex smallestIndex;

    smallestIndex = (LCHILD(index) < size && dc[colHeap[LCHILD(index)]] < dc[colHeap[index]]) ? LCHILD(index) : index;
    if(RCHILD(index) < size && dc[colHeap[RCHILD(index)]] < dc[colHeap[smallestIndex]]) {
        smallestIndex = RCHILD(index);
    }
    if(smallestIndex != index) {
    	colHeapInv[colHeap[index]] = smallestIndex;
    	colHeapInv[colHeap[smallestIndex]] = index;
		temp = colHeap[index];
    	colHeap[index] = colHeap[smallestIndex];
    	colHeap[smallestIndex] = temp;
		heapifyColCandidatesDown(colHeap, colHeapInv, size, smallestIndex, dc);
    }
}

void heapifyColCandidatesUp(MatrixIndex *colHeap, MatrixIndex *colHeapInv, MatrixIndex index, MatrixIndex *dc) {
	MatrixIndex temp = colHeap[index];

    while (index && dc[temp] < dc[colHeap[PARENT(index)]]) {
    	colHeapInv[colHeap[PARENT(index)]] = index;
        colHeap[index] = colHeap[PARENT(index)];
        index = PARENT(index);
    }
    colHeapInv[temp] = index;
    colHeap[index] = temp;
}

void insertColCandidate(MatrixIndex *colHeap, MatrixIndex *colHeapInv, MatrixIndex size, MatrixIndex col, MatrixIndex *dc) {
	MatrixIndex index = size;

	colHeap[index] = col;
	colHeapInv[col] = index;
	heapifyColCandidatesUp(colHeap, colHeapInv, index, dc);
}

void updateColCandidate(MatrixIndex *colHeap, MatrixIndex *colHeapInv, MatrixIndex size, MatrixIndex col, MatrixIndex *dc) {
	MatrixIndex index = colHeapInv[col];
	if (index == 0 || dc[colHeap[PARENT(index)]] < dc[colHeap[index]]) {
		heapifyColCandidatesDown(colHeap, colHeapInv, size, index, dc);
	} else {
		heapifyColCandidatesUp(colHeap, colHeapInv, index, dc);
	}
}

void deleteColCandidate(MatrixIndex *colHeap, MatrixIndex *colHeapInv, MatrixIndex size, MatrixIndex col, MatrixIndex *dc) {
	MatrixIndex index = colHeapInv[col];
	colHeapInv[colHeap[index]] = size - 1;
	colHeapInv[colHeap[size - 1]] = index;
	colHeap[index] = colHeap[--size];
	heapifyColCandidatesDown(colHeap, colHeapInv, size, index, dc);
}

void buildColCandidateHeap(MatrixIndex *colHeap, MatrixIndex *colHeapInv, MatrixIndex *dc, MatrixIndex size) {
	MatrixIndex i;

    for (i = 0; i < size; ++i) {
    	colHeap[i] = i;
    	colHeapInv[i] = i;
    }
    for (i = (size - 1) / 2 + 1; i > 0; --i) {
        heapifyColCandidatesDown(colHeap, colHeapInv, size, i - 1, dc);
    }
}

void insertSmallDc(MatrixIndex smallDcHeap[], int *smallDcCount, MatrixIndex *colHeap, MatrixIndex colHeapIndex, MatrixIndex *dc) {
	int i = (*smallDcCount)++ ;
	while (i && dc[colHeap[colHeapIndex]] < dc[colHeap[smallDcHeap[PARENT(i)]]]) {
		smallDcHeap[i] = smallDcHeap[PARENT(i)];
		i = PARENT(i);
	}
	smallDcHeap[i] = colHeapIndex;
}

void heapifySmallDcsDown(MatrixIndex smallDcHeap[], MatrixIndex smallDcCount, MatrixIndex index, MatrixIndex *colHeap, MatrixIndex *dc) {
	MatrixIndex temp;
    MatrixIndex smallestIndex;

    smallestIndex = (LCHILD(index) < smallDcCount && dc[colHeap[smallDcHeap[LCHILD(index)]]] < dc[colHeap[smallDcHeap[index]]]) ? LCHILD(index) : index;
    if(RCHILD(index) < smallDcCount && dc[colHeap[smallDcHeap[RCHILD(index)]]] < dc[colHeap[smallDcHeap[smallestIndex]]]) {
        smallestIndex = RCHILD(index);
    }
    if(smallestIndex != index) {
    	temp = smallDcHeap[index];
    	smallDcHeap[index] = smallDcHeap[smallestIndex];
    	smallDcHeap[smallestIndex] = temp;
    	heapifySmallDcsDown(smallDcHeap, smallDcCount, smallestIndex, colHeap, dc);
    }
}

MatrixIndex extractMinDc(MatrixIndex smallDcHeap[], int *smallDcCount, MatrixIndex *colHeap, MatrixIndex *dc) {
	MatrixIndex min = smallDcHeap[0];
	smallDcHeap[0] = smallDcHeap[--(*smallDcCount)] ;
	heapifySmallDcsDown(smallDcHeap, *smallDcCount, 0, colHeap, dc);
	return min;
}

int smallestDcs(MatrixIndex *colHeap, MatrixIndex size, MatrixIndex *dc, MatrixIndex result[]) {
	MatrixIndex smallDcHeap[NSRCH + 1], min;
	int smallDcCount, resultCount;

	smallDcCount = resultCount = 0;

	insertSmallDc(smallDcHeap, &smallDcCount, colHeap, 0, dc);
	while (resultCount < NSRCH && smallDcCount) {
		min = extractMinDc(smallDcHeap, &smallDcCount, colHeap, dc);
		result[resultCount++] = colHeap[min];
		if (LCHILD(min) < size) {
			insertSmallDc(smallDcHeap, &smallDcCount, colHeap, LCHILD(min), dc);
			if (RCHILD(min) < size) {
				insertSmallDc(smallDcHeap, &smallDcCount, colHeap, RCHILD(min), dc);
			}
		}
	}

	return resultCount;
}
