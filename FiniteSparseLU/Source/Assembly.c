/*
 * Assembly.c
 *
 *  Created on: Mar 16, 2017
 *      Author: plech
 */

#include <stdlib.h>
#include "Assembly.h"
#include "MinHeap.h"
#include "ElementLists.h"
#include "Utils.h"

void markRowDeletedInTraversal(ElementListNode *node, Element **elements, void *d) {
	int moduleCount = *(int *)d;
	markRowDeleted(elements, node, moduleCount);
}

void placePivotRow(Value ***F, MatrixIndex pivRow, MatrixIndex pivRowLocal, Value **Xi, MatrixIndex Ucount, SparseMatVal **AkRows, SparseMatVal **AkCols, MatrixIndex *AkRowCounts, MatrixIndex *AkColCounts, ElementListNode **rowElementLists, Element **elements, int moduleCount) {
	SparseMatVal *val, *nextVal;
	MatrixIndex j;
	int mod;

	for (j = 0; j < Ucount; ++j) {
		for (mod = 0; mod < moduleCount; ++mod) {
			F[mod][pivRowLocal][j] = Xi[mod][j];
		}
	}
	val = AkRows[pivRow];
	while (val) {
		if (val->prevInCol) {
			val->prevInCol->nextInCol = val->nextInCol;
			if (val->nextInCol) val->nextInCol->prevInCol = val->prevInCol;
		} else {
			AkCols[val->j] = val->nextInCol;
			if (val->nextInCol) val->nextInCol->prevInCol = 0;
		}
		--(AkColCounts[val->j]);
		nextVal = val->nextInRow;
		val->x = safe_free(val->x);
		val = safe_free(val);
		val = nextVal;
	}
	AkRows[pivRow] = 0;
	AkRowCounts[pivRow] = 0;
	traverseElementList(rowElementLists + pivRow, elements, &markRowDeletedInTraversal, &moduleCount, 0);
}

void markColDeletedInTraversal(ElementListNode *node, Element **elements, void *d) {
	int moduleCount = *(int *)d;
	markColDeleted(elements, node, moduleCount);
}

void placePivotCol(Value ***F, MatrixIndex pivCol, MatrixIndex pivColLocal, Value **Xj, MatrixIndex Lcount, SparseMatVal **AkRows, SparseMatVal **AkCols, MatrixIndex *AkRowCounts, MatrixIndex *AkColCounts, ElementListNode **colElementLists, Element **elements, int moduleCount) {
	SparseMatVal *val, *nextVal;
	MatrixIndex i;
	int mod;

	for (i = 0; i < Lcount; ++i) {
		for (mod = 0; mod < moduleCount; ++mod) {
			F[mod][i][pivColLocal] = Xj[mod][i];
		}
	}
	val = AkCols[pivCol];
	while (val) {
		if (val->prevInRow) {
			val->prevInRow->nextInRow = val->nextInRow;
			if (val->nextInRow) val->nextInRow->prevInRow = val->prevInRow;
		} else {
			AkRows[val->i] = val->nextInRow;
			if (val->nextInRow) val->nextInRow->prevInRow = 0;
		}
		--(AkRowCounts[val->i]);
		nextVal = val->nextInCol;
		val->x = safe_free(val->x);
		val = safe_free(val);
		val = nextVal;
	}
	AkCols[pivCol] = 0;
	AkColCounts[pivCol] = 0;
	traverseElementList(colElementLists + pivCol, elements, &markColDeletedInTraversal, &moduleCount, 1);
}

void assembleCol(Value ***F, MatrixIndex *U, MatrixIndex *L, MatrixIndex Lcount, SparseMatVal **AkRows, SparseMatVal **AkCols, MatrixIndex *AkRowCounts, MatrixIndex *AkColCounts, MatrixIndex j, Value *modules, int moduleCount) {
	SparseMatVal *val, *nextVal;
	MatrixIndex i;
	int mod;

	val = AkCols[U[j]];
	while (val) {
		for (i = 0; i < Lcount; ++i) {
			if (L[i] != val->i) continue;
			break;
		}
		for (mod = 0; mod < moduleCount; ++mod) {
			F[mod][i][j] = (F[mod][i][j] + val->x[mod]) % modules[mod];
		}
		if (val->prevInRow) {
			val->prevInRow->nextInRow = val->nextInRow;
			if (val->nextInRow) val->nextInRow->prevInRow = val->prevInRow;
		} else {
			AkRows[val->i] = val->nextInRow;
			if (val->nextInRow) val->nextInRow->prevInRow = 0;
		}
		--(AkRowCounts[val->i]);
		nextVal = val->nextInCol;
		val->x = safe_free(val->x);
		val = safe_free(val);
		val = nextVal;
	}
	AkCols[U[j]] = 0;
	AkColCounts[U[j]] = 0;
}

void assembleRow(Value ***F, MatrixIndex *U, MatrixIndex *L, MatrixIndex Ucount, SparseMatVal **AkRows, SparseMatVal **AkCols, MatrixIndex *AkRowCounts, MatrixIndex *AkColCounts, MatrixIndex i, Value *modules, int moduleCount) {
	SparseMatVal *val, *nextVal;
	MatrixIndex j;
	int mod;

	val = AkRows[L[i]];
	while (val) {
		for (j = 0; j < Ucount; ++j) {
			if (U[j] != val->j) continue;
			break;
		}
		for (mod = 0; mod < moduleCount; ++mod) {
			F[mod][i][j] = (F[mod][i][j] + val->x[mod]) % modules[mod];
		}
		if (val->prevInCol) {
			val->prevInCol->nextInCol = val->nextInCol;
			if (val->nextInCol) val->nextInCol->prevInCol = val->prevInCol;
		} else {
			AkCols[val->j] = val->nextInCol;
			if (val->nextInCol) val->nextInCol->prevInCol = 0;
		}
		--(AkColCounts[val->j]);
		nextVal = val->nextInRow;
		val->x = safe_free(val->x);
		val = safe_free(val);
		val = nextVal;
	}
	AkRows[L[i]] = 0;
	AkRowCounts[L[i]] = 0;
}

typedef struct ext_degree_action_t {

	MatrixIndex *wRow, *wCol, wZeroShift, *extTotal, *L, Lcount, *U, Ucount, i, j;
	Value ***F, *modules;
	int moduleCount;

} ExtDegreeActionData;

void performExtRowDegreeAction(ElementListNode *node, Element **elements, void *d) {
	MatrixIndex ext;
	ExtDegreeActionData *data = (ExtDegreeActionData *)d;

	ext = data->wRow[node->element] >= data->wZeroShift ? data->wRow[node->element] - data->wZeroShift : elements[node->element]->liveUeCount;
	if (ext) {
		*(data->extTotal) += ext;
	} else {
		assembleElementRow(elements, node, data->U, &(data->Ucount), 0, data->F, data->i, 0, 1, data->modules, data->moduleCount, 0, 0);
	}
}

void assembleRows(Value ***F, MatrixIndex *U, MatrixIndex Ucount, MatrixIndex *L, Bool *Lpivotal, MatrixIndex Lcount, SparseMatVal **AkRows, SparseMatVal **AkCols, MatrixIndex *AkRowCounts, MatrixIndex *AkColCounts, int pivots, ElementListNode **rowElementLists, Element **elements, MatrixIndex *wCol, MatrixIndex *wRow, MatrixIndex wZeroShift, MatrixIndex *dr, MatrixIndex colCount, MatrixIndex k, Value *modules, int moduleCount) {
	MatrixIndex i, j, alphaI, test, extTotal;
	SparseMatVal *val;
	ExtDegreeActionData data;
	int found;

	data.wRow = wRow;
	data.wZeroShift = wZeroShift;
	data.U = U;
	data.Ucount = Ucount;
	data.F = F;
	data.modules = modules;
	data.moduleCount = moduleCount;
	data.extTotal = &extTotal;

	for (i = 0; i < Lcount; ++i) {
		if (Lpivotal[i]) continue;
		alphaI = 0;
		val = AkRows[L[i]];
		while (val) {
			found = 0;
			for (j = 0; j < Ucount; ++j) {
				if (U[j] != val->j) continue;
				found = 1;
				break;
			}
			if (found) ++alphaI;
			else break;
			val = val->nextInRow;
		}
		extTotal = 0;
		data.i = i;
		traverseElementList(rowElementLists + L[i], elements, &performExtRowDegreeAction, &data, 0);
		dr[L[i]] += Ucount - pivots;
		test = colCount - k - 1;
		if (test < dr[L[i]]) dr[L[i]] = test;
		test = Ucount - pivots + AkRowCounts[L[i]] - alphaI + extTotal;
		if (test < dr[L[i]]) dr[L[i]] = test;
		if (alphaI == AkRowCounts[L[i]]) {
			assembleRow(F, U, L, Ucount, AkRows, AkCols, AkRowCounts, AkColCounts, i, modules, moduleCount);
		}
	}
}

void performExtColDegreeAction(ElementListNode *node, Element **elements, void *d) {
	MatrixIndex ext;
	ExtDegreeActionData *data = (ExtDegreeActionData *)d;

	ext = data->wCol[node->element] >= data->wZeroShift ? data->wCol[node->element] - data->wZeroShift : elements[node->element]->liveLeCount;
	if (ext) {
		*(data->extTotal) += ext;
	} else {
		if (data->wRow[node->element] >= data->wZeroShift ? data->wRow[node->element] - data->wZeroShift : elements[node->element]->liveUeCount) {
			assembleElementCol(elements, node, data->L, &(data->Lcount), 0, data->F, data->j, 0, 1, data->modules, data->moduleCount);
		}
	}
}

void assembleCols(MatrixIndex *colHeap, MatrixIndex *colHeapInv, Value ***F, MatrixIndex *U, Bool *Upivotal, MatrixIndex Ucount, MatrixIndex *L, MatrixIndex Lcount, SparseMatVal **AkRows, SparseMatVal **AkCols, MatrixIndex *AkColCounts, MatrixIndex *AkRowCounts, int pivots, ElementListNode **colElementLists, Element **elements, MatrixIndex *wCol, MatrixIndex *wRow, MatrixIndex wZeroShift, MatrixIndex *dc, MatrixIndex rowCount, MatrixIndex colCount, MatrixIndex k, Value *modules, int moduleCount) {
	MatrixIndex i, j, alphaJ, test, extTotal;
	SparseMatVal *val;
	ExtDegreeActionData data;
	int found;

	data.wRow = wRow;
	data.wCol = wCol;
	data.wZeroShift = wZeroShift;
	data.L = L;
	data.Lcount = Lcount;
	data.F = F;
	data.modules = modules;
	data.moduleCount = moduleCount;
	data.extTotal = &extTotal;

	for (j = 0; j < Ucount; ++j) {
		if (Upivotal[j]) continue;
		alphaJ = 0;
		val = AkCols[U[j]];
		while (val) {
			found = 0;
			for (i = 0; i < Lcount; ++i) {
				if (L[i] != val->i) continue;
				found = 1;
				break;
			}
			if (found) ++alphaJ;
			else break;
			val = val->nextInCol;
		}
		extTotal = 0;
		data.j = j;
		traverseElementList(colElementLists + U[j], elements, &performExtColDegreeAction, &data, 1);
		dc[U[j]] += Lcount - pivots;
		test = rowCount - k - 1;
		if (test < dc[U[j]]) dc[U[j]] = test;
		test = Lcount - pivots + AkColCounts[U[j]] - alphaJ + extTotal;
		if (test < dc[U[j]]) dc[U[j]] = test;
		updateColCandidate(colHeap, colHeapInv, colCount - k, U[j], dc);
		if (alphaJ == AkColCounts[U[j]]) {
			assembleCol(F, U, L, Lcount, AkRows, AkCols, AkRowCounts, AkColCounts, j, modules, moduleCount);
		}
	}
}

void assemblePrevious(MatrixIndex *colHeap, MatrixIndex *colHeapInv, Value ***F, MatrixIndex *U, MatrixIndex Ucount, MatrixIndex *L, MatrixIndex Lcount, Bool *Upivotal, Bool *Lpivotal, SparseMatVal **AkRows, SparseMatVal **AkCols, MatrixIndex *AkColCounts, MatrixIndex *AkRowCounts, int pivots, ElementListNode **rowElementLists, ElementListNode **colElementLists, Element **elements, MatrixIndex *wCol, MatrixIndex *wRow, MatrixIndex wZeroShift, MatrixIndex *dc, MatrixIndex *dr, MatrixIndex rowCount, MatrixIndex colCount, MatrixIndex k, Value *modules, int moduleCount) {
	assembleCols(colHeap, colHeapInv, F, U, Upivotal, Ucount, L, Lcount, AkRows, AkCols, AkColCounts, AkRowCounts, pivots, colElementLists, elements, wCol, wRow, wZeroShift, dc, rowCount, colCount, k, modules, moduleCount);
	assembleRows(F, U, Ucount, L, Lpivotal, Lcount, AkRows, AkCols, AkRowCounts, AkColCounts, pivots, rowElementLists, elements, wCol, wRow, wZeroShift, dr, colCount, k, modules, moduleCount);
}
