/*
 * GlobalPivotSearch.c
 *
 *  Created on: Mar 14, 2017
 *      Author: plech
 */

#include <limits.h>
#include <stdlib.h>
#include "GlobalPivotSearch.h"
#include "MinHeap.h"
#include "ElementLists.h"
#include "Utils.h"

#define GREATER_DC_THAN(a, b)		(dc[(a)] > dc[(b)])
#define SMALLER_DC_THAN(a, b)		(dc[(a)] < dc[(b)])

int assemblePivotColCandidates(MatrixIndex colCandidates[], int colCandidateCount, MatrixIndex *colHeap, MatrixIndex *colHeapInv, MatrixIndex colHeapSize, MatrixIndex *Lj[], Value **Xj[], MatrixIndex Lcounts[],
		SparseMatVal **AkCols, ElementListNode **colElementLists, Element **elements,
		MatrixIndex *dc, Value *modules, int moduleCount) {

	int j, mod;
	SparseMatVal *val;

	for (j = 0; j < colCandidateCount; ++j) {
		if (!(Lj[j] = safe_malloc(dc[colCandidates[j]], sizeof(MatrixIndex)))) return 1;
		if (!(Xj[j] = safe_calloc(moduleCount, sizeof(Value *)))) return 1;
		for (mod = 0; mod < moduleCount; ++mod) {
			if (!(Xj[j][mod] = safe_calloc(dc[colCandidates[j]], sizeof(Value)))) return 1;
		}
		Lcounts[j] = 0;
		val = AkCols[colCandidates[j]];
		while (val) {
			Lj[j][Lcounts[j]] = val->i;
			for (mod = 0; mod < moduleCount; ++mod) {
				Xj[j][mod][Lcounts[j]] = val->x[mod];
			}
			++Lcounts[j];
			val = val->nextInCol;
		}
		assembleElementCols(elements, colElementLists + colCandidates[j], Lj[j], Lcounts + j, Xj[j], 1, 0, modules, moduleCount);
		dc[colCandidates[j]] = Lcounts[j];
		updateColCandidate(colHeap, colHeapInv, colHeapSize, colCandidates[j], dc);
	}

	return 0;
}

int assemblePivotRow(MatrixIndex **Ui, Value ***Xi, MatrixIndex pivRow, MatrixIndex pivCol, MatrixIndex *pivColLocal,
		SparseMatVal **AkRows, ElementListNode **rowElementLists, Element **elements,
		MatrixIndex *Ucount, MatrixIndex *dr, Value *modules, int moduleCount, MatrixIndex size) {

	SparseMatVal *val;
	int mod;

	if (!(*Ui = safe_malloc(dr[pivRow], sizeof(MatrixIndex)))) return 1;
	if (!(*Xi = safe_calloc(moduleCount, sizeof(Value *)))) return 1;
	for (mod = 0; mod < moduleCount; ++mod) {
		if (!((*Xi)[mod] = safe_calloc(dr[pivRow], sizeof(Value)))) return 1;
	}
	*pivColLocal = size;
	*Ucount = 0;
	val = AkRows[pivRow];
	while (val) {
		(*Ui)[*Ucount] = val->j;
		for (mod = 0; mod < moduleCount; ++mod) {
			(*Xi)[mod][*Ucount] = val->x[mod];
		}
		if (val->j == pivCol) *pivColLocal = *Ucount;
		++(*Ucount);
		val = val->nextInRow;
	}
	assembleElementRows(elements, rowElementLists + pivRow, *Ui, Ucount, *Xi, 1, 0, modules, moduleCount, pivCol, pivColLocal);
	dr[pivRow] = *Ucount;

	return *pivColLocal == size;
}

int checkForZero(MatrixIndex i, int moduleCount, Value **Xj) {
	int mod;

	for (mod = 0; mod < moduleCount; ++mod) {
		if (Xj[mod][i] == 0) return 1;
	}
	return 0;
}

int findSeedPivot(MatrixIndex *colCandidates, int colCandidateCount, MatrixIndex *Lj[], Value **Xj[], MatrixIndex Lcounts[], MatrixIndex *dr,
		MatrixIndex *pivRow, MatrixIndex *pivRowLocal, MatrixIndex *pivCol, int moduleCount) {

	int j, foundJ = -1;
	MatrixIndex i, cost, minCost;

	for (j = 0; j < colCandidateCount; ++j) {
		for (i = 0; i < Lcounts[j]; ++i) {
			if (checkForZero(i, moduleCount, Xj[j])) continue;
			cost = (dr[Lj[j][i]] - 1) * (Lcounts[j] - 1);
			if (foundJ < 0 || cost < minCost) {
				minCost = cost;
				*pivRow = Lj[j][i];
				*pivRowLocal = i;
				*pivCol = colCandidates[j];
				foundJ = j;
			}
		}
	}

	return foundJ;
}
