/*
 * FactorFrontal.c
 *
 *  Created on: Mar 8, 2017
 *      Author: plech
 */

#include <sys/time.h>
#include <stdio.h>
#include "FactorFrontal.h"
#include "Utils.h"

TaskDescriptor task;

Value calculatePivotInverse(int step) {
	return calculateInverse(task.pivotBlock[step * task.pivotCount + step], task.module);
}

void updatePivotCol(int step, Value inverse) {
	int i;

	for (i = step + 1; i < task.pivotCount; ++i) {
		task.pivotBlock[i * task.pivotCount + step] = (task.pivotBlock[i * task.pivotCount + step] * inverse) % task.module;
	}
}

void updateRemainingSubmatrix(int step) {
	int i, j;

	for (i = step + 1; i < task.pivotCount; ++i) {
		for (j = step + 1; j < task.pivotCount; ++j) {
			task.pivotBlock[i * task.pivotCount + j] = (task.module + task.pivotBlock[i * task.pivotCount + j] - ((task.pivotBlock[step * task.pivotCount + j] * task.pivotBlock[i * task.pivotCount + step]) % task.module)) % task.module;
		}
	}
}

int factorPivotBlockHost(Value inverses[]) {
	int step;

	for (step = 0; step < task.pivotCount; ++step) {
		if (step && !task.pivotBlock[step * task.pivotCount + step]) {
			return step;
		}
		inverses[step] = calculatePivotInverse(step);
		updatePivotCol(step, inverses[step]);
		updateRemainingSubmatrix(step);
	}
	return 0;
}

void updatePivotRowsAndColsHost(Value inverses[]) {
	int rightSide, step, i;

	for (rightSide = 0; rightSide < task.contributionBlockCols; ++rightSide) {
		for (step = 0; step < task.pivotCount; ++step) {
			for (i = 0; i < step; ++i) {
				task.pivotRowBlock[step * task.contributionBlockCols + rightSide] = (task.module + task.pivotRowBlock[step * task.contributionBlockCols + rightSide] - ((task.pivotBlock[step * task.pivotCount + i] * task.pivotRowBlock[i * task.contributionBlockCols + rightSide]) % task.module)) % task.module;
			}
		}
	}

	for (rightSide = 0; rightSide < task.contributionBlockRows; ++rightSide) {
		for (step = 0; step < task.pivotCount; ++step) {
			for (i = 0; i < step; ++i) {
				task.pivotColBlock[rightSide * task.pivotCount + step] = (task.module + task.pivotColBlock[rightSide * task.pivotCount + step] - ((task.pivotBlock[i * task.pivotCount + step] * task.pivotColBlock[rightSide * task.pivotCount + i]) % task.module)) % task.module;
			}
			task.pivotColBlock[rightSide * task.pivotCount + step] = (task.pivotColBlock[rightSide * task.pivotCount + step] * inverses[step]) % task.module;
		}
	}
}

void updateContributionBlockHost() {
	int i, j, step;
	Value decrement;

	for (i = 0; i < task.contributionBlockRows; ++i) {
		for (j = 0; j < task.contributionBlockCols; ++j) {
			decrement = 0;
			for (step = 0; step < task.pivotCount; ++step) {
				decrement = (decrement + ((task.pivotRowBlock[step * task.contributionBlockCols + j] * task.pivotColBlock[i * task.pivotCount + step]) % task.module)) % task.module;
			}
			task.contributionBlock[i * task.contributionBlockCols + j] = (task.module + task.contributionBlock[i * task.contributionBlockCols + j] - decrement) % task.module;
		}
	}
}

int factorFrontalMatrix(TaskDescriptor *taskQueue, int taskCount) {
	Value inverses[PIVOT_BLOCK_SIZE];
#ifdef DEBUG
	struct timeval t1, t2;
	double elapsedTime;
#endif
	int result, i, minResult;

#ifdef DEBUG
	gettimeofday(&t1, 0);
#endif
	minResult = PIVOT_BLOCK_SIZE;
	for (i = 0; i < taskCount; ++i) {
		task = taskQueue[i];
		result = factorPivotBlockHost(inverses);
		if (result) {
			if (result < minResult) minResult = result;
			continue;
		}
		updatePivotRowsAndColsHost(inverses);
		updateContributionBlockHost();
	}
#ifdef DEBUG
	gettimeofday(&t2, 0);
#endif

	result = minResult < PIVOT_BLOCK_SIZE ? minResult : 0;
#ifdef DEBUG
	printf("Total result is %d.\n", result);

	elapsedTime = (t2.tv_sec - t1.tv_sec) * 1000;
	elapsedTime += ((double) t2.tv_usec - (double) t1.tv_usec) / 1000;
	printf("Computation time: %.3f ms.\n", elapsedTime);
#endif

	return result;
}
