/*
 * FiniteSparseLU.cu
 *
 *  Created on: Feb 25, 2017
 *      Author: plech
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <inttypes.h>
#include "FactorFrontal.h"
#include "FiniteSparseLU.h"
#include "MatrixConversions.h"
#include "GlobalPivotSearch.h"
#include "ElementLists.h"
#include "Assembly.h"
#include "LocalPivotSearch.h"
#include "MinHeap.h"
#include "Debug.h"
#include "FactorizeWrapper.h"
#include "Utils.h"

int allocMemory(SparseMatVal*** AkRows, SparseMatVal*** AkCols, Element*** elements, ElementListNode ***rowElementLists, ElementListNode ***colElementLists, MatrixIndex** dr, MatrixIndex** dc, MatrixIndex **colHeap, MatrixIndex **colHeapInv, MatrixIndex** AkRowCounts, MatrixIndex** AkColCounts, MatrixIndex **wRow, MatrixIndex **wCol, MatrixIndex rowCount, MatrixIndex colCount, MatrixIndex **Louti, ValueIndex **Loutp, Value ***Loutx, MatrixIndex **Uouti, ValueIndex **Uoutp, Value ***Uoutx, MatrixIndex **rowPermOut, MatrixIndex **rowPermInvOut, MatrixIndex **colPermOut, MatrixIndex **colPermInvOut, ValueIndex *allocatedLvalueCount, ValueIndex *allocatedUvalueCount, ValueIndex valueCount, int moduleCount) {
	int mod;

	*allocatedLvalueCount = *allocatedUvalueCount = valueCount * 2;

	if (!(*AkRows = safe_calloc(rowCount, sizeof(SparseMatVal*)))) return 1;
	if (!(*AkCols = safe_calloc(colCount, sizeof(SparseMatVal*)))) return 1;
	if (!(*elements = safe_calloc(colCount, sizeof(Element*)))) return 1;
	if (!(*rowElementLists = safe_calloc(rowCount, sizeof(ElementListNode *)))) return 1;
	if (!(*colElementLists = safe_calloc(colCount, sizeof(ElementListNode *)))) return 1;
	if (!(*dr = safe_calloc(rowCount, sizeof(MatrixIndex)))) return 1;
	if (!(*dc = safe_calloc(colCount, sizeof(MatrixIndex)))) return 1;
	if (!(*colHeap = safe_malloc(colCount, sizeof(MatrixIndex)))) return 1;
	if (!(*colHeapInv = safe_malloc(colCount, sizeof(MatrixIndex)))) return 1;
	if (!(*AkRowCounts = safe_malloc(rowCount, sizeof(MatrixIndex)))) return 1;
	if (!(*AkColCounts = safe_malloc(colCount, sizeof(MatrixIndex)))) return 1;
	if (!(*wRow = safe_calloc(colCount, sizeof(MatrixIndex)))) return 1;
	if (!(*wCol = safe_calloc(colCount, sizeof(MatrixIndex)))) return 1;
	if (!(*Loutp = safe_malloc((colCount + 1), sizeof(ValueIndex)))) return 1;
	if (!(*Louti = safe_malloc(*allocatedLvalueCount, sizeof(MatrixIndex)))) return 1;
	if (!(*Loutx = safe_calloc(moduleCount, sizeof(Value *)))) return 1;
	for (mod = 0; mod < moduleCount; ++mod) {
		if (!((*Loutx)[mod] = safe_malloc(*allocatedLvalueCount, sizeof(Value)))) return 1;
	}
	if (!(*Uoutp = safe_malloc((colCount + 1), sizeof(ValueIndex)))) return 1;
	if (!(*Uouti = safe_malloc(*allocatedUvalueCount, sizeof(MatrixIndex)))) return 1;
	if (!(*Uoutx = safe_calloc(moduleCount, sizeof(Value *)))) return 1;
	for (mod = 0; mod < moduleCount; ++mod) {
		if (!((*Uoutx)[mod] = safe_malloc(*allocatedUvalueCount, sizeof(Value)))) return 1;
	}
	if (!(*rowPermOut = safe_malloc(colCount, sizeof(MatrixIndex)))) return 1;
	if (!(*rowPermInvOut = safe_calloc(rowCount, sizeof(MatrixIndex)))) return 1;
	if (!(*colPermOut = safe_malloc(colCount, sizeof(MatrixIndex)))) return 1;
	if (!(*colPermInvOut = safe_calloc(colCount, sizeof(MatrixIndex)))) return 1;
	return 0;
}

int allocFrontal(Value ****F, Bool **Lpivotal, Bool **Upivotal, MatrixIndex *s, MatrixIndex *t, MatrixIndex Lcount, MatrixIndex Ucount, int moduleCount) {
	MatrixIndex i;
	int mod;

	*s = RELAX * Lcount;
	*t = RELAX * Ucount;
	if (!(*F = safe_calloc(moduleCount, sizeof(Value **)))) return 1;
	for (mod = 0; mod < moduleCount; ++mod) {
		if (!((*F)[mod] = safe_calloc(*s, sizeof(Value *)))) return 1;
		for (i = 0; i < *s; ++i) {
			if (!((*F)[mod][i] = safe_calloc(*t, sizeof(Value)))) return 1;
		}
	}
	if (!(*Lpivotal = safe_calloc(*s, sizeof(Bool)))) return 1;
	if (!(*Upivotal = safe_calloc(*t, sizeof(Bool)))) return 1;
	return 0;
}

void freeFrontalMemory(

		MatrixIndex *LjCand[],
		Value **XjCand[],
		MatrixIndex **L, MatrixIndex **U,
		Bool **Lpivotal, Bool **Upivotal,
		Value ***Xi, Value ***Xj,
		Value ****F,
		int moduleCount,
		MatrixIndex s

		) {

	MatrixIndex i;
	int mod;

	for (i = 0; i < NSRCH; ++i) {
		LjCand[i] = safe_free(LjCand[i]);
		XjCand[i] = safe_free_incl_sublevel((void **)(XjCand[i]), moduleCount);
	}
	*L = safe_free(*L);
	*U = safe_free(*U);
	*Lpivotal = safe_free(*Lpivotal);
	*Upivotal = safe_free(*Upivotal);
	*Xi = safe_free_incl_sublevel((void **)*Xi, moduleCount);
	*Xj = safe_free_incl_sublevel((void **)*Xj, moduleCount);
	if (*F) {
		for (mod = 0; mod < moduleCount; ++mod) {
			if ((*F)[mod]) {
				for (i = 0; i < s; ++i) {
					(*F)[mod][i] = safe_free((*F)[mod][i]);
				}
				(*F)[mod] = safe_free((*F)[mod]);
			}
		}
		*F = safe_free(*F);
	}
}

void freeOutputMemory(

		ValueIndex **Loutp, MatrixIndex **Louti, Value ***Loutx,
		ValueIndex **Uoutp, MatrixIndex **Uouti, Value ***Uoutx,
		MatrixIndex **rowPermOut, MatrixIndex **rowPermInvOut,
		MatrixIndex **colPermOut, MatrixIndex **colPermInvOut,
		int moduleCount

		) {

	*Loutp = safe_free(*Loutp);
	*Louti = safe_free(*Louti);
	*Loutx = safe_free_incl_sublevel((void **)*Loutx, moduleCount);
	*Uoutp = safe_free(*Uoutp);
	*Uouti = safe_free(*Uouti);
	*Uoutx = safe_free_incl_sublevel((void **)*Uoutx, moduleCount);
	*rowPermOut = safe_free(*rowPermOut);
	*colPermOut = safe_free(*colPermOut);
	*rowPermInvOut = safe_free(*rowPermInvOut);
	*colPermInvOut = safe_free(*colPermInvOut);
}

void freeWorkspaceMemory(

		SparseMatVal ***AkRows, SparseMatVal ***AkCols,
		MatrixIndex **AkRowCounts, MatrixIndex **AkColCounts,
		Element ***elements,
		ElementListNode ***rowElementLists, ElementListNode ***colElementLists,
		MatrixIndex **dc, MatrixIndex **dr,
		MatrixIndex **colHeap, MatrixIndex **colHeapInv,
		MatrixIndex **wRow, MatrixIndex **wCol,
		MatrixIndex rowCount, MatrixIndex colCount,
		int moduleCount

		) {

	freePointerBased(AkRows, AkCols, rowCount, colCount);
	*AkRowCounts = safe_free(*AkRowCounts);
	*AkColCounts = safe_free(*AkColCounts);
	*elements = freeElements(*elements, colCount, moduleCount);
	*rowElementLists = freeElementLists(*rowElementLists, rowCount);
	*colElementLists = freeElementLists(*colElementLists, colCount);
	*dc = safe_free(*dc);
	*dr = safe_free(*dr);
	*colHeap = safe_free(*colHeap);
	*colHeapInv = safe_free(*colHeapInv);
	*wRow = safe_free(*wRow);
	*wCol = safe_free(*wCol);
}

void freeAllMemoryIncludingOutput(

		SparseMatVal ***AkRows, SparseMatVal ***AkCols,
		MatrixIndex **AkRowCounts, MatrixIndex **AkColCounts,
		Element ***elements,
		ElementListNode ***rowElementLists, ElementListNode ***colElementLists,
		MatrixIndex **dc, MatrixIndex **dr,
		MatrixIndex **colHeap, MatrixIndex **colHeapInv,
		MatrixIndex *LjCand[],
		Value **XjCand[],
		MatrixIndex **L, MatrixIndex **U,
		Bool **Lpivotal, Bool **Upivotal,
		Value ***Xi, Value ***Xj,
		Value ****F,
		MatrixIndex **wRow, MatrixIndex **wCol,
		ValueIndex **Loutp, MatrixIndex **Louti, Value ***Loutx,
		ValueIndex **Uoutp, MatrixIndex **Uouti, Value ***Uoutx,
		MatrixIndex **rowPermOut, MatrixIndex **rowPermInvOut,
		MatrixIndex **colPermOut, MatrixIndex **colPermInvOut,
		MatrixIndex rowCount, MatrixIndex colCount,
		MatrixIndex s,
		int moduleCount

		) {

	freeFrontalMemory(LjCand, XjCand, L, U, Lpivotal, Upivotal, Xi, Xj, F, moduleCount, s);
	freeOutputMemory(Loutp, Louti, Loutx, Uoutp, Uouti, Uoutx, rowPermOut, rowPermInvOut, colPermOut, colPermInvOut, moduleCount);
	freeWorkspaceMemory(AkRows, AkCols, AkRowCounts, AkColCounts, elements, rowElementLists, colElementLists, dc, dr, colHeap, colHeapInv, wRow, wCol, rowCount, colCount, moduleCount);
}

void freeAllMemory(

		SparseMatVal ***AkRows, SparseMatVal ***AkCols,
		MatrixIndex **AkRowCounts, MatrixIndex **AkColCounts,
		Element ***elements,
		ElementListNode ***rowElementLists, ElementListNode ***colElementLists,
		MatrixIndex **dc, MatrixIndex **dr,
		MatrixIndex **colHeap, MatrixIndex **colHeapInv,
		MatrixIndex *LjCand[],
		Value **XjCand[],
		MatrixIndex **L, MatrixIndex **U,
		Bool **Lpivotal, Bool **Upivotal,
		Value ***Xi, Value ***Xj,
		Value ****F,
		MatrixIndex **wRow, MatrixIndex **wCol,
		MatrixIndex rowCount, MatrixIndex colCount,
		MatrixIndex s,
		int moduleCount

		) {

	freeFrontalMemory(LjCand, XjCand, L, U, Lpivotal, Upivotal, Xi, Xj, F, moduleCount, s);
	freeWorkspaceMemory(AkRows, AkCols, AkRowCounts, AkColCounts, elements, rowElementLists, colElementLists, dc, dr, colHeap, colHeapInv, wRow, wCol, rowCount, colCount, moduleCount);
}

void changeDegrees(MatrixIndex *L, MatrixIndex Lcount, Bool *Lpivotal, MatrixIndex *dr, MatrixIndex *U, MatrixIndex Ucount, Bool *Upivotal, MatrixIndex *dc, MatrixIndex *colHeap, MatrixIndex *colHeapInv, MatrixIndex colHeapSize, int change) {
	MatrixIndex i, j;

	for (i = 0; i < Lcount; ++i) {
		if (Lpivotal[i]) continue;
		dr[L[i]] += change;
	}
	for (j = 0; j < Ucount; ++j) {
		if (Upivotal[j]) continue;
		dc[U[j]] += change;
		updateColCandidate(colHeap, colHeapInv, colHeapSize, U[j], dc);
	}
}

int finiteSparseLU(Value *modules, int moduleCount, MatrixIndex *A3i, MatrixIndex *A3j, Value **A3x, MatrixIndex rowCount, MatrixIndex colCount, ValueIndex valueCount, ValueIndex **Loutp, MatrixIndex **Louti, Value ***Loutx, ValueIndex **Uoutp, MatrixIndex **Uouti, Value ***Uoutx, MatrixIndex **rowPermOut, MatrixIndex **rowPermInvOut, MatrixIndex **colPermOut, MatrixIndex **colPermInvOut) {

	SparseMatVal **AkRows = 0, **AkCols = 0;
	MatrixIndex *AkRowCounts = 0, *AkColCounts = 0;
	MatrixIndex k, e, s = 0, t;
	Element **elements = 0;
	ElementListNode **rowElementLists = 0, **colElementLists = 0;
	MatrixIndex *dc = 0, *dr = 0;
	MatrixIndex *colHeap = 0, *colHeapInv = 0;
	MatrixIndex colCandidates[NSRCH];
	MatrixIndex *LjCand[NSRCH] = {0};
	Value **XjCand[NSRCH] = {0};
	MatrixIndex LcountCand[NSRCH];
	MatrixIndex *L = 0, *U = 0;
	Bool *Lpivotal = 0, *Upivotal = 0;
	Value **Xi = 0, **Xj = 0;
	MatrixIndex pivRow, pivCol, pivRowLocal[PIVOT_BLOCK_SIZE], pivColLocal[PIVOT_BLOCK_SIZE];
	Value ***F = 0;
	MatrixIndex Lcount, Ucount, oldLcount, oldUcount;
	MatrixIndex *wRow = 0, *wCol = 0;
	MatrixIndex wMax, wZeroShift;
	ValueIndex allocatedLvalueCount, allocatedUvalueCount;
	int i, j, pivots, colCandidateCount, factorizeResult, mod;
#ifdef TIME
	SparseLUTime time;
#endif

	if (moduleCount <= 0) {
		fprintf(stderr, "Module count must be at least one, not %d.\n", moduleCount);
		freeAllMemoryIncludingOutput(&AkRows, &AkCols, &AkRowCounts, &AkColCounts, &elements, &rowElementLists, &colElementLists, &dc, &dr, &colHeap, &colHeapInv, LjCand, XjCand, &L, &U, &Lpivotal, &Upivotal, &Xi, &Xj, &F, &wRow, &wCol, Loutp, Louti, Loutx, Uoutp, Uouti, Uoutx, rowPermOut, rowPermInvOut, colPermOut, colPermInvOut, rowCount, colCount, s, moduleCount);
		return 1;
	}
	for (mod = 0; mod < moduleCount; ++mod) {
		if (modules[mod] > (((Value) 1) << 32)) {
			fprintf(stderr, "Module at index %d (%"PRIu64") is larger than 2^32.\n", mod, modules[mod]);
			freeAllMemoryIncludingOutput(&AkRows, &AkCols, &AkRowCounts, &AkColCounts, &elements, &rowElementLists, &colElementLists, &dc, &dr, &colHeap, &colHeapInv, LjCand, XjCand, &L, &U, &Lpivotal, &Upivotal, &Xi, &Xj, &F, &wRow, &wCol, Loutp, Louti, Loutx, Uoutp, Uouti, Uoutx, rowPermOut, rowPermInvOut, colPermOut, colPermInvOut, rowCount, colCount, s, moduleCount);
			return 1;
		}
	}
	if (rowCount < colCount) {
		fprintf(stderr, "The input matrix must have at least as many rows as it has columns. Incorrect values are rowCount: %"PRIu64", colCount: %"PRIu64".\n", rowCount, colCount);
		freeAllMemoryIncludingOutput(&AkRows, &AkCols, &AkRowCounts, &AkColCounts, &elements, &rowElementLists, &colElementLists, &dc, &dr, &colHeap, &colHeapInv, LjCand, XjCand, &L, &U, &Lpivotal, &Upivotal, &Xi, &Xj, &F, &wRow, &wCol, Loutp, Louti, Loutx, Uoutp, Uouti, Uoutx, rowPermOut, rowPermInvOut, colPermOut, colPermInvOut, rowCount, colCount, s, moduleCount);
		return 1;
	}
	if (rowCount >= (((Value) 1) << 32)) {
		fprintf(stderr, "Number of rows of the input matrix cannot exceed 2^32 - 1. Incorrect value: %"PRIu64".\n", rowCount);
		freeAllMemoryIncludingOutput(&AkRows, &AkCols, &AkRowCounts, &AkColCounts, &elements, &rowElementLists, &colElementLists, &dc, &dr, &colHeap, &colHeapInv, LjCand, XjCand, &L, &U, &Lpivotal, &Upivotal, &Xi, &Xj, &F, &wRow, &wCol, Loutp, Louti, Loutx, Uoutp, Uouti, Uoutx, rowPermOut, rowPermInvOut, colPermOut, colPermInvOut, rowCount, colCount, s, moduleCount);
		return 1;
	}
	if (colCount >= (((Value) 1) << 32)) {
		fprintf(stderr, "Number of columns of the input matrix cannot exceed 2^32 - 1. Incorrect value: %"PRIu64".\n", colCount);
		freeAllMemoryIncludingOutput(&AkRows, &AkCols, &AkRowCounts, &AkColCounts, &elements, &rowElementLists, &colElementLists, &dc, &dr, &colHeap, &colHeapInv, LjCand, XjCand, &L, &U, &Lpivotal, &Upivotal, &Xi, &Xj, &F, &wRow, &wCol, Loutp, Louti, Loutx, Uoutp, Uouti, Uoutx, rowPermOut, rowPermInvOut, colPermOut, colPermInvOut, rowCount, colCount, s, moduleCount);
		return 1;
	}

#ifdef TIME
	memset(&time, 0, sizeof(SparseLUTime));
	elapsedTimeStart(&time.total);
	elapsedTimeStart(&time.Outer.init);
#endif
	if (allocMemory(&AkRows, &AkCols, &elements, &rowElementLists, &colElementLists, &dr, &dc, &colHeap, &colHeapInv, &AkRowCounts, &AkColCounts, &wRow, &wCol, rowCount, colCount, Louti, Loutp, Loutx, Uouti, Uoutp, Uoutx, rowPermOut, rowPermInvOut, colPermOut, colPermInvOut, &allocatedLvalueCount, &allocatedUvalueCount, valueCount, moduleCount)) {
		fprintf(stderr, "Couldn't allocate space for LU factorization.\n");
		freeAllMemoryIncludingOutput(&AkRows, &AkCols, &AkRowCounts, &AkColCounts, &elements, &rowElementLists, &colElementLists, &dc, &dr, &colHeap, &colHeapInv, LjCand, XjCand, &L, &U, &Lpivotal, &Upivotal, &Xi, &Xj, &F, &wRow, &wCol, Loutp, Louti, Loutx, Uoutp, Uouti, Uoutx, rowPermOut, rowPermInvOut, colPermOut, colPermInvOut, rowCount, colCount, s, moduleCount);
		return 1;
	}
	if (tripletToPointerBased(A3i, A3j, A3x, valueCount, rowCount, colCount, AkRows, AkCols, dr, dc, modules, moduleCount)) {
		fprintf(stderr, "Couldn't interpret input matrix.\n");
		freeAllMemoryIncludingOutput(&AkRows, &AkCols, &AkRowCounts, &AkColCounts, &elements, &rowElementLists, &colElementLists, &dc, &dr, &colHeap, &colHeapInv, LjCand, XjCand, &L, &U, &Lpivotal, &Upivotal, &Xi, &Xj, &F, &wRow, &wCol, Loutp, Louti, Loutx, Uoutp, Uouti, Uoutx, rowPermOut, rowPermInvOut, colPermOut, colPermInvOut, rowCount, colCount, s, moduleCount);
		return 1;
	}
	memcpy(AkRowCounts, dr, rowCount * sizeof(MatrixIndex));
	memcpy(AkColCounts, dc, colCount * sizeof(MatrixIndex));
#ifdef DEBUG
	printSparse(AkRows, AkCols, AkRowCounts, AkColCounts, rowCount, colCount, modules, moduleCount);
#endif
	buildColCandidateHeap(colHeap, colHeapInv, dc, colCount);
#ifdef DEBUG
	printColHeap(colHeap, colHeapInv, dc, colCount, colCount);
#endif
	(*Loutp)[0] = 0;
	(*Uoutp)[0] = 0;
	wMax = 0;
	k = 0;
#ifdef TIME
	elapsedTimeEnd(&time.Outer.init);
#endif
	while (k < colCount) {
#ifdef TIME
		elapsedTimeStart(&time.Outer.frontalPreAssembly);
#endif
		e = k;
		wZeroShift = wMax + 1; // all w(e)'s are now below wZeroShift ("< 0")
#if defined(DEBUG) || defined(INFO)
		printf("Starting main loop, k = %"PRIu64", e = %"PRIu64".\n", k, e);
#endif
		colCandidateCount = smallestDcs(colHeap, colCount - k, dc, colCandidates);
#ifdef DEBUG
		printColCandidates(colCandidates, colCandidateCount, dc);
#endif
		if (assemblePivotColCandidates(colCandidates, colCandidateCount, colHeap, colHeapInv, colCount - k, LjCand, XjCand, LcountCand, AkCols, colElementLists, elements, dc, modules, moduleCount)) {
			fprintf(stderr, "Couldn't allocate space for seed pivot column candidates at step %"PRIu64".\n", k);
			freeAllMemoryIncludingOutput(&AkRows, &AkCols, &AkRowCounts, &AkColCounts, &elements, &rowElementLists, &colElementLists, &dc, &dr, &colHeap, &colHeapInv, LjCand, XjCand, &L, &U, &Lpivotal, &Upivotal, &Xi, &Xj, &F, &wRow, &wCol, Loutp, Louti, Loutx, Uoutp, Uouti, Uoutx, rowPermOut, rowPermInvOut, colPermOut, colPermInvOut, rowCount, colCount, s, moduleCount);
			return 1;
		}
#ifdef DEBUG
		printf("Col heap after pre-assembling pivot col candidates:\n\n");
		printColHeap(colHeap, colHeapInv, dc, colCount - k, colCount);
#endif
		if ((j = findSeedPivot(colCandidates, colCandidateCount, LjCand, XjCand, LcountCand, dr, &pivRow, pivRowLocal, &pivCol, moduleCount)) < 0) {
			fprintf(stderr, "Couldn't find a seed pivot at step %"PRIu64".\n", k);
			freeAllMemoryIncludingOutput(&AkRows, &AkCols, &AkRowCounts, &AkColCounts, &elements, &rowElementLists, &colElementLists, &dc, &dr, &colHeap, &colHeapInv, LjCand, XjCand, &L, &U, &Lpivotal, &Upivotal, &Xi, &Xj, &F, &wRow, &wCol, Loutp, Louti, Loutx, Uoutp, Uouti, Uoutx, rowPermOut, rowPermInvOut, colPermOut, colPermInvOut, rowCount, colCount, s, moduleCount);
			return 1;
		}
		Lcount = LcountCand[j];
		L = LjCand[j];
		LjCand[j] = 0;
		Xj = XjCand[j];
		XjCand[j] = 0;
		if (assemblePivotRow(&U, &Xi, pivRow, pivCol, pivColLocal, AkRows, rowElementLists, elements, &Ucount, dr, modules, moduleCount, colCount)) {
			fprintf(stderr, "Couldn't assemble pivot row, not enough memory or input matrix is rank deficient at step %"PRIu64".\n", k);
			freeAllMemoryIncludingOutput(&AkRows, &AkCols, &AkRowCounts, &AkColCounts, &elements, &rowElementLists, &colElementLists, &dc, &dr, &colHeap, &colHeapInv, LjCand, XjCand, &L, &U, &Lpivotal, &Upivotal, &Xi, &Xj, &F, &wRow, &wCol, Loutp, Louti, Loutx, Uoutp, Uouti, Uoutx, rowPermOut, rowPermInvOut, colPermOut, colPermInvOut, rowCount, colCount, s, moduleCount);
			return 1;
		}
		if (allocFrontal(&F, &Lpivotal, &Upivotal, &s, &t, Lcount, Ucount, moduleCount)) {
			fprintf(stderr, "Couldn't allocate space for frontal matrix at step %"PRIu64".\n", k);
			freeAllMemoryIncludingOutput(&AkRows, &AkCols, &AkRowCounts, &AkColCounts, &elements, &rowElementLists, &colElementLists, &dc, &dr, &colHeap, &colHeapInv, LjCand, XjCand, &L, &U, &Lpivotal, &Upivotal, &Xi, &Xj, &F, &wRow, &wCol, Loutp, Louti, Loutx, Uoutp, Uouti, Uoutx, rowPermOut, rowPermInvOut, colPermOut, colPermInvOut, rowCount, colCount, s, moduleCount);
			return 1;
		}
#ifdef DEBUG
		printf("Picked candidate %d. Pivot row, col: %"PRIu64", %"PRIu64". Locally: %"PRIu64", %"PRIu64". Assembled col:\n", j, pivRow, pivCol, pivRowLocal[0], pivColLocal[0]);
		printAssembledForm(L, Xj, Lcount, modules, moduleCount);
		printf("Assembled row:\n");
		printAssembledForm(U, Xi, Ucount, modules, moduleCount);
		printFrontal(F, Lcount, Ucount, modules, moduleCount);
#endif
		oldLcount = oldUcount = 0;
#ifdef TIME
		elapsedTimeEnd(&time.Outer.frontalPreAssembly);
		elapsedTimeStart(&time.Outer.frontalLoop);
#endif
		for (pivots = 1; k < colCount; ++pivots) {
#if defined(DEBUG) || defined(INFO)
			printf("Starting local pivot loop nr. %d\n", pivots);
#endif
			(*rowPermOut)[k] = pivRow;
			(*rowPermInvOut)[pivRow] = k;
			(*colPermOut)[k] = pivCol;
			(*colPermInvOut)[pivCol] = k;
			Lpivotal[pivRowLocal[pivots - 1]] = 1;
			Upivotal[pivColLocal[pivots - 1]] = 1;
			if (Lcount > oldLcount || Ucount > oldUcount) {
#ifdef TIME
				elapsedTimeStart(&time.FrontalLoop.pivotPlacement);
#endif
				if (Lcount > oldLcount) placePivotCol(F, pivCol, pivColLocal[pivots - 1], Xj, Lcount, AkRows, AkCols, AkRowCounts, AkColCounts, colElementLists, elements, moduleCount);
				if (Ucount > oldUcount) placePivotRow(F, pivRow, pivRowLocal[pivots - 1], Xi, Ucount, AkRows, AkCols, AkRowCounts, AkColCounts, rowElementLists, elements, moduleCount);
#ifdef TIME
				elapsedTimeEnd(&time.FrontalLoop.pivotPlacement);
#endif
#ifdef DEBUG
				printf("After placing pivot col and row into frontal:\n");
				printSparse(AkRows, AkCols, AkRowCounts, AkColCounts, rowCount, colCount, modules, moduleCount);
				printFrontal(F, Lcount, Ucount, modules, moduleCount);
				printElements(elements, rowElementLists, colElementLists, rowCount, colCount, modules, moduleCount);
#endif
#ifdef TIME
				elapsedTimeStart(&time.FrontalLoop.extDegreeCalc);
#endif
				calculateExternalColDegrees(L, oldLcount, Lcount, rowElementLists, elements, wCol, wZeroShift, &wMax);
				calculateExternalRowDegrees(U, oldUcount, Ucount, colElementLists, elements, wRow, wZeroShift, &wMax);
#ifdef TIME
				elapsedTimeEnd(&time.FrontalLoop.extDegreeCalc);
#endif
#ifdef DEBUG
				printExternalDegrees(wCol, wRow, wZeroShift, colCount, elements);
#endif
#ifdef TIME
				elapsedTimeStart(&time.FrontalLoop.otherAssembly);
#endif
				assemblePrevious(colHeap, colHeapInv, F, U, Ucount, L, Lcount, Upivotal, Lpivotal, AkRows, AkCols, AkColCounts, AkRowCounts, pivots, rowElementLists, colElementLists, elements, wCol, wRow, wZeroShift, dc, dr, rowCount, colCount, k, modules, moduleCount);
#ifdef TIME
				elapsedTimeEnd(&time.FrontalLoop.otherAssembly);
#endif
#ifdef DEBUG
				printf("After assembling other cols and rows into frontal:\n");
				printSparse(AkRows, AkCols, AkRowCounts, AkColCounts, rowCount, colCount, modules, moduleCount);
				printFrontal(F, Lcount, Ucount, modules, moduleCount);
				printElements(elements, rowElementLists, colElementLists, rowCount, colCount, modules, moduleCount);
#endif
			} else {
				changeDegrees(L, Lcount, Lpivotal, dr, U, Ucount, Upivotal, dc, colHeap, colHeapInv, colCount - k, -1);
			}
			deleteColCandidate(colHeap, colHeapInv, colCount - k, pivCol, dc);
			k++;
			oldLcount = Lcount;
			oldUcount = Ucount;
#ifdef DEBUG
			printf("State of the col heap after updating dcs and deleting the pivCol.\n");
			printColHeap(colHeap, colHeapInv, dc, colCount - k, colCount);
#endif
#ifdef TIME
			elapsedTimeStart(&time.FrontalLoop.findLocal);
#endif
			if (pivots == PIVOT_BLOCK_SIZE
					|| findLocalCandidateCol(U, Ucount, Upivotal, dc, &pivCol, pivColLocal + pivots, pivots)
					|| findLocalCandidateRow(L, Lcount, Lpivotal, dr, &pivRow, pivRowLocal + pivots, pivots)) {
#ifdef TIME
				elapsedTimeEnd(&time.FrontalLoop.findLocal);
#endif
				++pivots;
				break;
			}
#ifdef TIME
			elapsedTimeEnd(&time.FrontalLoop.findLocal);
#endif
#if defined(DEBUG) || defined(INFO)
			printf("Local candidate col. Global: %"PRIu64", local: %"PRIu64".\n", pivCol, pivColLocal[pivots]);
			printf("Local candidate row. Global: %"PRIu64", local: %"PRIu64".\n", pivRow, pivRowLocal[pivots]);
#endif
			if (dc[pivCol] > Lcount - pivots) {
#ifdef TIME
				elapsedTimeStart(&time.FrontalLoop.assembleLocal);
#endif
				if (assembleLocalCandidateCol(F, &L, &Xj, &Lcount, pivCol, pivColLocal[pivots], AkCols, colElementLists, elements, colHeap, colHeapInv, dc, colCount, pivots, k, modules, moduleCount)) {
					fprintf(stderr, "Couldn't allocate space for local candiadte column at step %"PRIu64".\n", k);
					freeAllMemoryIncludingOutput(&AkRows, &AkCols, &AkRowCounts, &AkColCounts, &elements, &rowElementLists, &colElementLists, &dc, &dr, &colHeap, &colHeapInv, LjCand, XjCand, &L, &U, &Lpivotal, &Upivotal, &Xi, &Xj, &F, &wRow, &wCol, Loutp, Louti, Loutx, Uoutp, Uouti, Uoutx, rowPermOut, rowPermInvOut, colPermOut, colPermInvOut, rowCount, colCount, s, moduleCount);
					return 1;
				}
#ifdef TIME
				elapsedTimeEnd(&time.FrontalLoop.assembleLocal);
#endif
#ifdef DEBUG
				printf("Assembled next local candidate col, here it is, plus the updated col heap based on updated dc's:\n");
				printAssembledForm(L, Xj, Lcount, modules, moduleCount);
				printColHeap(colHeap, colHeapInv, dc, colCount - k, colCount);
#endif
				if (Lcount > s) {
#ifdef DEBUG
					printf("Lcount = %"PRIu64" is more than s = %"PRIu64", stopping this frontal matrix.\n", Lcount, s);
#endif
					++pivots;
					break;
				}
			}
			if (dr[pivRow] > Ucount - pivots) {
#ifdef TIME
				elapsedTimeStart(&time.FrontalLoop.assembleLocal);
#endif
				if (assembleLocalCandidateRow(F, &U, &Xi, &Ucount, pivRow, pivRowLocal[pivots], AkRows, rowElementLists, elements, dr, pivots, modules, moduleCount)) {
					fprintf(stderr, "Couldn't allocate space for local candidate row at step %"PRIu64".\n", k);
					freeAllMemoryIncludingOutput(&AkRows, &AkCols, &AkRowCounts, &AkColCounts, &elements, &rowElementLists, &colElementLists, &dc, &dr, &colHeap, &colHeapInv, LjCand, XjCand, &L, &U, &Lpivotal, &Upivotal, &Xi, &Xj, &F, &wRow, &wCol, Loutp, Louti, Loutx, Uoutp, Uouti, Uoutx, rowPermOut, rowPermInvOut, colPermOut, colPermInvOut, rowCount, colCount, s, moduleCount);
					return 1;
				}
#ifdef TIME
				elapsedTimeEnd(&time.FrontalLoop.assembleLocal);
#endif
#ifdef DEBUG
				printf("Assembled next local candidate row, here it is:\n");
				printAssembledForm(U, Xi, Ucount, modules, moduleCount);
#endif
				if (Ucount > t) {
#ifdef DEBUG
					printf("Ucount = %"PRIu64" is more than t = %"PRIu64", stopping this frontal matrix.\n", Ucount, t);
#endif
					++pivots;
					break;
				}
			}
		}
#ifdef TIME
		elapsedTimeEnd(&time.Outer.frontalLoop);
#endif
#if defined(DEBUG) || defined(INFO)
		printf("Starting factorization routine with pivot count %d.\n", pivots - 1);
#endif
#ifdef TIME
		elapsedTimeStart(&time.Outer.frontalFactor);
#endif
		factorizeResult = factorizeWrapperExec(F, L, Lpivotal, U, Upivotal, pivots - 1, oldLcount, oldUcount, rowElementLists, colElementLists, elements, Loutp, Louti, Loutx, Uoutp, Uouti, Uoutx, modules, moduleCount, e, &allocatedLvalueCount, &allocatedUvalueCount, pivRowLocal, pivColLocal);
		if (factorizeResult < 0) {
			fprintf(stderr, "Failed to factorize frontal matrix at step %"PRIu64".\n", k);
			freeAllMemoryIncludingOutput(&AkRows, &AkCols, &AkRowCounts, &AkColCounts, &elements, &rowElementLists, &colElementLists, &dc, &dr, &colHeap, &colHeapInv, LjCand, XjCand, &L, &U, &Lpivotal, &Upivotal, &Xi, &Xj, &F, &wRow, &wCol, Loutp, Louti, Loutx, Uoutp, Uouti, Uoutx, rowPermOut, rowPermInvOut, colPermOut, colPermInvOut, rowCount, colCount, s, moduleCount);
			return 1;
		} else if (factorizeResult) {
#if defined(DEBUG) || defined(INFO)
			printf("Factorization failed with a zero pivot nr. %d. Un-pivoting remaining pivots to ensure success and re-factorizing.\n", factorizeResult);
#endif
			changeDegrees(L, oldLcount, Lpivotal, dr, U, oldUcount, Upivotal, dc, colHeap, colHeapInv, colCount - k, pivots - 1 - factorizeResult);
			for (i = factorizeResult; i < pivots - 1; ++i) {
				dr[L[pivRowLocal[i]]] = oldUcount - factorizeResult;
				dc[U[pivColLocal[i]]] = oldLcount - factorizeResult;
				insertColCandidate(colHeap, colHeapInv, colCount - k, U[pivColLocal[i]], dc);
				k--;
				Lpivotal[pivRowLocal[i]] = 0;
				Upivotal[pivColLocal[i]] = 0;
			}
			factorizeResult = factorizeWrapperExec(F, L, Lpivotal, U, Upivotal, factorizeResult, oldLcount, oldUcount, rowElementLists, colElementLists, elements, Loutp, Louti, Loutx, Uoutp, Uouti, Uoutx, modules, moduleCount, e, &allocatedLvalueCount, &allocatedUvalueCount, pivRowLocal, pivColLocal);
			if (factorizeResult < 0) {
				fprintf(stderr, "Failed to re-factorize frontal matrix at step %"PRIu64".\n", k);
				freeAllMemoryIncludingOutput(&AkRows, &AkCols, &AkRowCounts, &AkColCounts, &elements, &rowElementLists, &colElementLists, &dc, &dr, &colHeap, &colHeapInv, LjCand, XjCand, &L, &U, &Lpivotal, &Upivotal, &Xi, &Xj, &F, &wRow, &wCol, Loutp, Louti, Loutx, Uoutp, Uouti, Uoutx, rowPermOut, rowPermInvOut, colPermOut, colPermInvOut, rowCount, colCount, s, moduleCount);
				return 1;
			}
		}
#ifdef DEBUG
		printf("After frontal factorization, any new elements?:\n");
		printElements(elements, rowElementLists, colElementLists, rowCount, colCount, modules, moduleCount);
#endif
		freeFrontalMemory(LjCand, XjCand, &L, &U, &Lpivotal, &Upivotal, &Xi, &Xj, &F, moduleCount, s);
#ifdef TIME
		elapsedTimeEnd(&time.Outer.frontalFactor);
#endif
#ifdef DEBUG
		printf("Output L:");
		printCompressedForm(*Loutp, *Louti, *Loutx, k, modules, moduleCount);
		printf("Output U:");
		printCompressedForm(*Uoutp, *Uouti, *Uoutx, k, modules, moduleCount);
#endif
	}
#ifdef TIME
	elapsedTimeStart(&time.Outer.finalize);
#endif
	translateOldToNewIndices(*Louti, *Loutx, (*Loutp)[colCount], *rowPermInvOut, *rowPermOut, modules, moduleCount, colCount);
	translateOldToNewIndices(*Uouti, *Uoutx, (*Uoutp)[colCount], *colPermInvOut, 0, modules, moduleCount, 0);
	freeAllMemory(&AkRows, &AkCols, &AkRowCounts, &AkColCounts, &elements, &rowElementLists, &colElementLists, &dc, &dr, &colHeap, &colHeapInv, LjCand, XjCand, &L, &U, &Lpivotal, &Upivotal, &Xi, &Xj, &F, &wRow, &wCol, rowCount, colCount, s, moduleCount);
#ifdef TIME
	elapsedTimeEnd(&time.Outer.finalize);
	elapsedTimeEnd(&time.total);
	printLUTime(time);
#endif
	return 0;
}
