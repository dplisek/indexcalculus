#include "MatrixConversions.h"
#include "Utils.h"
#include <stdlib.h>
#include <inttypes.h>

int tripletToPointerBased(MatrixIndex *A3i, MatrixIndex *A3j, Value **A3x, ValueIndex valueCount, MatrixIndex rowCount, MatrixIndex colCount,
		SparseMatVal **rows, SparseMatVal **cols, MatrixIndex *dr, MatrixIndex *dc, Value *modules, int moduleCount) {

	ValueIndex val;
	SparseMatVal *new;
	int mod;

	for (val = 0; val < valueCount; ++val) {
		if (!(new = safe_calloc(1, sizeof(SparseMatVal)))) {
			fprintf(stderr, "Not enough memory.\n");
			return 1;
		}
		if (rows[A3i[val]]) {
			new->nextInRow = rows[A3i[val]];
			rows[A3i[val]]->prevInRow = new;
		}
		rows[A3i[val]] = new;
		if (cols[A3j[val]]) {
			new->nextInCol = cols[A3j[val]];
			cols[A3j[val]]->prevInCol = new;
		}
		cols[A3j[val]] = new;
		new->i = A3i[val];
		new->j = A3j[val];
		if (new->i >= rowCount) {
			fprintf(stderr, "Row index %"PRIu64" is beyond matrix.\n", new->i);
			return 1;
		}
		if (new->j >= colCount) {
			fprintf(stderr, "Column index %"PRIu64" is beyond matrix.\n", new->j);
			return 1;
		}
		if (!(new->x = safe_malloc(moduleCount, sizeof(Value)))) {
			fprintf(stderr, "Not enough memory.\n");
			return 1;
		}
		for (mod = 0; mod < moduleCount; ++mod) {
			new->x[mod] = A3x[mod][val] % modules[mod];
		}
		dr[new->i]++;
		dc[new->j]++;
	}

	return 0;
}

void translateOldToNewIndices(MatrixIndex *i, Value **x, ValueIndex valueCount, MatrixIndex *permInv, MatrixIndex *permCheck, Value *modules, int moduleCount, MatrixIndex onePastIndex) {
	ValueIndex val;
	int mod;

	for (val = 0; val < valueCount; ++val) {
		if (x[0][val] < modules[0]) continue;
		if (permCheck && permCheck[permInv[i[val]]] != i[val]) {
			i[val] = onePastIndex;
		} else {
			i[val] = permInv[i[val]];
			for (mod = 0; mod < moduleCount; ++mod) {
				x[mod][val] -= modules[mod];
			}
		}
	}
}

void freePointerBased(SparseMatVal ***rows, SparseMatVal ***cols, MatrixIndex rowCount, MatrixIndex colCount) {
	MatrixIndex i, j;
	SparseMatVal *thisVal, *nextVal;

	if (*rows) {
		for (i = 0; i < rowCount; ++i) {
			thisVal = (*rows)[i];
			while (thisVal) {
				nextVal = thisVal->nextInRow;
				thisVal->x = safe_free(thisVal->x);
				thisVal = safe_free(thisVal);
				thisVal = nextVal;
			}
			(*rows)[i] = 0;
		}
	} else if (*cols) {
		for (j = 0; j < colCount; ++j) {
			thisVal = (*cols)[j];
			while (thisVal) {
				nextVal = thisVal->nextInCol;
				thisVal->x = safe_free(thisVal->x);
				thisVal = safe_free(thisVal);
				thisVal = nextVal;
			}
			(*cols)[j] = 0;
		}
	}
	*rows = safe_free(*rows);
	*cols = safe_free(*cols);
}
