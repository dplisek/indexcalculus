/*
 * PrepareFactorize.h
 *
 *  Created on: Mar 24, 2017
 *      Author: plech
 */

#ifndef PREPAREFACTORIZE_H_
#define PREPAREFACTORIZE_H_

#include "types.h"

int factorizeWrapperExec(Value ***F, MatrixIndex *L, Bool *Lpivotal, MatrixIndex *U, Bool *Upivotal, int pivots, MatrixIndex Lcount, MatrixIndex Ucount, ElementListNode **rowElementLists, ElementListNode **colElementLists, Element** elements, ValueIndex **Loutp, MatrixIndex **Louti, Value ***Loutx, ValueIndex **Uoutp, MatrixIndex **Uouti, Value ***Uoutx, Value *modules, int moduleCount, MatrixIndex elementOffset, ValueIndex *allocatedLvalueCount, ValueIndex *allocatedUvalueCount, MatrixIndex *pivotRowLocal, MatrixIndex *pivotColLocal);

#endif /* PREPAREFACTORIZE_H_ */
