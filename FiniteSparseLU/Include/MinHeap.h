/*
 * MinHeap.h
 *
 *  Created on: Mar 17, 2017
 *      Author: plech
 */

#ifndef MINHEAP_H_
#define MINHEAP_H_

#include "types.h"

void buildColCandidateHeap(MatrixIndex *colHeap, MatrixIndex *colHeapInv, MatrixIndex *dc, MatrixIndex size);
void updateColCandidate(MatrixIndex *colHeap, MatrixIndex *colHeapInv, MatrixIndex size, MatrixIndex col, MatrixIndex *dc);
void deleteColCandidate(MatrixIndex *colHeap, MatrixIndex *colHeapInv, MatrixIndex size, MatrixIndex col, MatrixIndex *dc);
void insertColCandidate(MatrixIndex *colHeap, MatrixIndex *colHeapInv, MatrixIndex size, MatrixIndex col, MatrixIndex *dc);

int smallestDcs(MatrixIndex *colHeap, MatrixIndex size, MatrixIndex *dc, MatrixIndex *result);

#endif /* MINHEAP_H_ */
