/*
 * Utils.h
 *
 *  Created on: Mar 31, 2017
 *      Author: plech
 */

#ifndef UTILS_H_
#define UTILS_H_

#include <stdlib.h>
#include <stdio.h>
#include <sys/time.h>
#include "types.h"

#define MAX(a, b) (((a) > (b)) ? (a) : (b))

Value calculateInverse(Value input, Value module);
void elapsedTimeStart(Time *time);
void elapsedTimeEnd(Time *time);
void *safe_free_incl_sublevel(void **p, int count);
FILE *safe_fclose(FILE *f);
// The following functions are copyright of Tim Davis
void *safe_malloc (size_t n, size_t size);
void *safe_calloc (size_t n, size_t size);
void *safe_free (void *p);
void *safe_realloc (void *p, size_t n, size_t size, Bool *ok);

#endif /* UTILS_H_ */
