/*
 * TripletToColumn.h
 *
 *  Created on: Mar 13, 2017
 *      Author: plech
 */

#ifndef TRIPLETTOCOLUMN_H_
#define TRIPLETTOCOLUMN_H_

#include "types.h"

int tripletToPointerBased(MatrixIndex *A3i, MatrixIndex *A3j, Value **A3x, ValueIndex valueCount, MatrixIndex rowCount, MatrixIndex colCount,
		SparseMatVal **rows, SparseMatVal **cols, MatrixIndex *dr, MatrixIndex *dc, Value *modules, int moduleCount);

void translateOldToNewIndices(MatrixIndex *i, Value **x, ValueIndex valueCount, MatrixIndex *permInv, MatrixIndex *permCheck, Value *modules, int moduleCount, MatrixIndex onePastIndex);

void freePointerBased(SparseMatVal ***rows, SparseMatVal ***cols, MatrixIndex rowCount, MatrixIndex colCount);

#endif /* TRIPLETTOCOLUMN_H_ */
