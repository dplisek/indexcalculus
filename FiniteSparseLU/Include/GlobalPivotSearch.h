/*
 * GlobalPivotSearch.h
 *
 *  Created on: Mar 14, 2017
 *      Author: plech
 */

#ifndef GLOBALPIVOTSEARCH_H_
#define GLOBALPIVOTSEARCH_H_

#include "types.h"

int assemblePivotColCandidates(MatrixIndex *colCandidates, int colCandidateCount, MatrixIndex *colHeap, MatrixIndex *colHeapInv, MatrixIndex colHeapSize, MatrixIndex *Lj[], Value **Xj[], MatrixIndex Lcounts[],
		SparseMatVal **AkCols, ElementListNode **colElementLists, Element **elements,
		MatrixIndex *dc, Value *modules, int moduleCount);

int assemblePivotRow(MatrixIndex **Ui, Value ***Xi, MatrixIndex pivRow, MatrixIndex pivCol, MatrixIndex *pivColLocal,
		SparseMatVal **AkRows, ElementListNode **rowElementLists, Element **elements,
		MatrixIndex *Ucount, MatrixIndex *dr, Value *modules, int moduleCount, MatrixIndex size);

int findSeedPivot(MatrixIndex *colCandidates, int colCandidateCount, MatrixIndex *Lj[], Value **Xj[], MatrixIndex Lcounts[], MatrixIndex *dr,
		MatrixIndex *pivRow, MatrixIndex *pivRowLocal, MatrixIndex *pivCol, int moduleCount);

#endif /* GLOBALPIVOTSEARCH_H_ */
