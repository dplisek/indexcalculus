/*
 * Debug.h
 *
 *  Created on: Mar 21, 2017
 *      Author: plech
 */

#ifndef DEBUG_H_
#define DEBUG_H_

#include "types.h"
#include <stdio.h>

void printSparseMathematicaFriendly(FILE *file, MatrixIndex *Ai, MatrixIndex *Aj, Value *Ax, ValueIndex valueCount);

void printCompressedColSquareSparseMathematicaFriendly(FILE *file, ValueIndex *Ap, MatrixIndex *Ai, Value *Ax, MatrixIndex size);

void printCompressedRowSparseMathematicaFriendly(FILE *file, ValueIndex *Ap, MatrixIndex *Ai, Value *Ax, MatrixIndex size);

void printRowPermMathematicaFriendly(FILE *file, MatrixIndex *rowPerm, MatrixIndex size);

void printColPermMathematicaFriendly(FILE *file, MatrixIndex *colPerm, MatrixIndex size);

void printVectorMathematicaFriendly(FILE *file, Value *x, MatrixIndex size);

void printSparse(SparseMatVal **rows, SparseMatVal **cols, MatrixIndex *rowCounts, MatrixIndex *colCounts, MatrixIndex rowCount, MatrixIndex colCount, Value *modules, int moduleCount);

void printColHeap(MatrixIndex *colHeap, MatrixIndex *colHeapInv, MatrixIndex *dc, MatrixIndex heapSize, MatrixIndex totalSize);

void printColCandidates(MatrixIndex *colCandidates, int count, MatrixIndex *dc);

void printAssembledForm(MatrixIndex *indices, Value **values, MatrixIndex size, Value *modules, int moduleCount);

void printBlock(Value *block, MatrixIndex rows, MatrixIndex cols);

void printFrontal(Value ***F, MatrixIndex Lcount, MatrixIndex Ucount, Value *modules, int moduleCount);

void printExternalDegrees(MatrixIndex *wCol, MatrixIndex *wRow, MatrixIndex wZeroShift, MatrixIndex size, Element **elements);

void printCompressedForm(ValueIndex *p, MatrixIndex *i, Value **x, MatrixIndex size, Value *modules, int moduleCount);

void printElements(Element **elements, ElementListNode **rowElementLists, ElementListNode **colElementLists, MatrixIndex rowCount, MatrixIndex colCount, Value *modules, int moduleCount);

void printLUTime(SparseLUTime time);

void printSolveTime(SolveLUTime time);

#endif /* DEBUG_H_ */
