/*
 * SolveLU.h
 *
 *  Created on: Mar 30, 2017
 *      Author: plech
 */

#ifndef SOLVELU_H_
#define SOLVELU_H_

#include "types.h"

// Solve a linear system provided a L and U matrix.
// Works simultaneously within a list of specified modules.
// The matrix can be rectangular with more rows than cols to ensure full rank, but the linear equations cannot conflict with each other.
//
// The input L and U matrices are provided in column and row compressed form, respectively,
// where values are specified once for each module, while indices are common.
// The module specification is at top level, e.g. Lx[mod][val].
//
// The input permutations are in default and inverse direction.
// Default: perm[newIndex] = oldIndex
// Inverse: permInv[oldIndex] = newIndex
//
// The vector b serves as the right-hand side on input, and as the solution x on output.
// The values are specified once for each module.
// The module specification is at top level, e.g. b[mod][val].
int solveLU(

		Value *modules, int moduleCount,					// Input List of modules to work in
		ValueIndex *Lp, MatrixIndex *Li, Value **Lx, 		// Input L matrix in column compressed form
		ValueIndex *Up, MatrixIndex *Ui, Value **Ux,		// Input U matrix in row compressed form
		MatrixIndex rowCount, MatrixIndex colCount,			// Input matrix dimensions
		MatrixIndex *rowPerm, MatrixIndex *rowPermInv, 		// Input Row permutation both ways
		MatrixIndex *colPerm, 								// Input Column permutation

		Value **b 											// Input Right-side vector b
															// ---> Output result x

);

#endif /* SOLVELU_H_ */
