/*
 * Assembly.h
 *
 *  Created on: Mar 16, 2017
 *      Author: plech
 */

#ifndef ASSEMBLY_H_
#define ASSEMBLY_H_

#include "types.h"

void placePivotRow(Value ***F, MatrixIndex pivRow, MatrixIndex pivRowLocal, Value **Xi, MatrixIndex Ucount, SparseMatVal **AkRows, SparseMatVal **AkCols, MatrixIndex *AkRowCounts, MatrixIndex *AkColCounts, ElementListNode **rowElementLists, Element **elements, int moduleCount);

void placePivotCol(Value ***F, MatrixIndex pivCol, MatrixIndex pivColLocal, Value **Xj, MatrixIndex Lcount, SparseMatVal **AkRows, SparseMatVal **AkCols, MatrixIndex *AkRowCounts, MatrixIndex *AkColCounts, ElementListNode **colElementLists, Element **elements, int moduleCount);

void assemblePrevious(MatrixIndex *colHeap, MatrixIndex *colHeapInv, Value ***F, MatrixIndex *U, MatrixIndex Ucount, MatrixIndex *L, MatrixIndex Lcount, Bool *Upivotal, Bool *Lpivotal, SparseMatVal **AkRows, SparseMatVal **AkCols, MatrixIndex *AkColCounts, MatrixIndex *AkRowCounts, int pivots, ElementListNode **rowElementLists, ElementListNode **colElementLists, Element **elements, MatrixIndex *wCol, MatrixIndex *wRow, MatrixIndex wZeroShift, MatrixIndex *dc, MatrixIndex *dr, MatrixIndex rowCount, MatrixIndex colCount, MatrixIndex k, Value *modules, int moduleCount);

#endif /* ASSEMBLY_H_ */
