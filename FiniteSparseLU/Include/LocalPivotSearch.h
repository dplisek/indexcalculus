/*
 * LocalPivotSearch.h
 *
 *  Created on: Mar 17, 2017
 *      Author: plech
 */

#ifndef LOCALPIVOTSEARCH_H_
#define LOCALPIVOTSEARCH_H_

#include "types.h"

int findLocalCandidateCol(MatrixIndex *U, MatrixIndex Ucount, char *Upivotal, MatrixIndex *dc, MatrixIndex *pivCol, MatrixIndex *pivColLocal, int pivots);

int findLocalCandidateRow(MatrixIndex *L, MatrixIndex Lcount, char *Lpivotal, MatrixIndex *dr, MatrixIndex *pivRow, MatrixIndex *pivRowLocal, int pivots);

int assembleLocalCandidateCol(Value ***F, MatrixIndex **L, Value ***Xj, MatrixIndex *Lcount, MatrixIndex pivCol, MatrixIndex pivColLocal,
		SparseMatVal **AkCols, ElementListNode **colElementLists, Element **elements,
		MatrixIndex *colHeap, MatrixIndex *colHeapInv, MatrixIndex *dc, MatrixIndex size, int localPivots, MatrixIndex globalPivots, Value *modules, int moduleCount);

int assembleLocalCandidateRow(Value ***F, MatrixIndex **U, Value ***Xi, MatrixIndex *Ucount, MatrixIndex pivRow, MatrixIndex pivRowLocal,
		SparseMatVal **AkRows, ElementListNode **rowElementLists, Element **elements, MatrixIndex *dr, int pivots, Value *module, int moduleCount);

#endif /* LOCALPIVOTSEARCH_H_ */
