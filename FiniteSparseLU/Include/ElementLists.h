/*
 * ElementLists.h
 *
 *  Created on: Mar 15, 2017
 *      Author: plech
 */

#ifndef ELEMENTLISTS_H_
#define ELEMENTLISTS_H_

void traverseElementList(ElementListNode **startNode, Element **elements, void (*work)(ElementListNode *, Element **, void *), void *data, int colwise);

void calculateExternalColDegrees(MatrixIndex *L, MatrixIndex oldLcount, MatrixIndex Lcount, ElementListNode **rowElementLists, Element **elements,
		MatrixIndex *wCol, MatrixIndex wZeroShift, MatrixIndex *wMax);

void calculateExternalRowDegrees(MatrixIndex *U, MatrixIndex oldUcount, MatrixIndex Ucount, ElementListNode **colElementLists, Element **elements,
		MatrixIndex *wRow, MatrixIndex wZeroShift, MatrixIndex *wMax);

void *freeElement(Element *element, int moduleCount);

void *freeElements(Element **elements, MatrixIndex elementCount, int moduleCount);

void *freeElementLists(ElementListNode **elementLists, MatrixIndex listCount);

void markColDeleted(Element **elements, ElementListNode *node, int moduleCount);

void assembleElementCol(Element **elements, ElementListNode *node, MatrixIndex *L, MatrixIndex *Lcount, Value **Xj, Value ***F, MatrixIndex j, Bool appending, Bool deleting, Value *modules, int moduleCount);

void assembleElementCols(Element **elements, ElementListNode **startNode, MatrixIndex *L, MatrixIndex *Lcount, Value **Xj, Bool appending, Bool deleting, Value *modules, int moduleCount);

void markRowDeleted(Element **elements, ElementListNode *node, int moduleCount);

void assembleElementRow(Element **elements, ElementListNode *node, MatrixIndex *U, MatrixIndex *Ucount, Value **Xi, Value ***F, MatrixIndex i, Bool appending, Bool deleting, Value *modules, int moduleCount, MatrixIndex pivCol, MatrixIndex *pivColLocal);

void assembleElementRows(Element **elements, ElementListNode **startNode, MatrixIndex *U, MatrixIndex *Ucount, Value **Xi, Bool appending, Bool deleting, Value *modules, int moduleCount, MatrixIndex pivCol, MatrixIndex *pivColLocal);

#endif /* ELEMENTLISTS_H_ */
