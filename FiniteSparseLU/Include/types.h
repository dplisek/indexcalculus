/*
 * types.h
 *
 *  Created on: Feb 24, 2017
 *      Author: plech
 */

#ifndef TYPES_H_
#define TYPES_H_

#include <stdint.h>
#include <sys/time.h>

#define RELAX							2
#define NSRCH							4

#define NUMTHREADS						1024
#define PIVOT_BLOCK_SIZE				16
#define PIVOT_ROW_COL_SEGMENT_SIZE		128
#define CONTRIBUTION_BLOCK_SEG_COLS		32
#define CONTRIBUTION_BLOCK_SEG_ROWS		64
#define CONTRIBUTION_BLOCK_ALTERATIONS	2

typedef uint64_t ValueIndex;
typedef uint64_t MatrixIndex;
typedef uint64_t Value;
typedef char Bool;

typedef struct sparse_mat_val_t SparseMatVal;

struct sparse_mat_val_t {

	MatrixIndex i, j;
	Value *x;
	SparseMatVal *nextInCol;
	SparseMatVal *nextInRow;
	SparseMatVal *prevInCol;
	SparseMatVal *prevInRow;

};

typedef struct element_t {

	MatrixIndex *Le, *Ue;
	Bool *LeDeleted, *UeDeleted;
	MatrixIndex LeCount, UeCount;
	MatrixIndex liveLeCount, liveUeCount;
	Value **contributionBlock;

} Element;

typedef struct element_list_node_t ElementListNode;

struct element_list_node_t {

	MatrixIndex element;
	MatrixIndex localIndex;
	ElementListNode *nextNode;
};

typedef struct task_descriptor_t {

	Value *pivotBlock;
	Value *pivotRowBlock;
	Value *pivotColBlock;
	Value *contributionBlock;
	int pivotCount;
	MatrixIndex contributionBlockRows;
	MatrixIndex contributionBlockCols;
	Value module;
	int *result;

} TaskDescriptor;

typedef struct shared_memory_t {

	TaskDescriptor task;

	union {
		struct {
			Value pivotBlock[PIVOT_BLOCK_SIZE][PIVOT_BLOCK_SIZE + 1];
			Value invertedPivots[PIVOT_BLOCK_SIZE];
			Value rowsSegment[PIVOT_BLOCK_SIZE][PIVOT_ROW_COL_SEGMENT_SIZE + 1];
			Value colsSegment[PIVOT_ROW_COL_SEGMENT_SIZE][PIVOT_BLOCK_SIZE + 1];
		} UpdatePivots;

		struct {
			Value rowsSegments[CONTRIBUTION_BLOCK_ALTERATIONS][PIVOT_BLOCK_SIZE][CONTRIBUTION_BLOCK_SEG_COLS + 1];
			Value colsSegments[CONTRIBUTION_BLOCK_ALTERATIONS][CONTRIBUTION_BLOCK_SEG_ROWS][PIVOT_BLOCK_SIZE + 1];
		} UpdateContributionBlock;

	} Data;

} SharedMemory;

typedef struct sparse_time_t {

	struct timeval t1, t2, elapsed;

} Time;

typedef struct sparse_lu_time_t {

	Time total;

	struct {
		Time init;
		Time frontalPreAssembly;
		Time frontalLoop;
		Time frontalFactor;
		Time finalize;
	} Outer;

	struct {
		Time pivotPlacement;
		Time extDegreeCalc;
		Time otherAssembly;
		Time findLocal;
		Time assembleLocal;
	} FrontalLoop;

} SparseLUTime;

typedef struct solve_lu_time_t {

	Time total;
	Time rowPerm;
	Time lower;
	Time upper;
	Time colPerm;

} SolveLUTime;

#endif /* TYPES_H_ */
