/*
 * FiniteSparseLU.h
 *
 *  Created on: Feb 25, 2017
 *      Author: plech
 */

#ifndef FINITESPARSELU_H_
#define FINITESPARSELU_H_

#include "types.h"

// Factorize a sparse matrix A into an L and a U matrix.
// Works simultaneously within a list of specified modules.
// The matrix can be rectangular with more rows than cols to ensure full rank, but the linear equations cannot conflict with each other.
//
// The input A matrix is provided in triplet form, where values are specified once for each module, while indices are common.
// The module specification is at top level, e.g. A3x[mod][val].
// The output matrices L and U are in column and row compressed form, respectively, and the same rule applies to their values.
//
// The output permutations are in default and inverse direction.
// Default: perm[newIndex] = oldIndex
// Inverse: permInv[oldIndex] = newIndex
int finiteSparseLU(

		Value *modules, int moduleCount, 										// Input List of modules to work in
		MatrixIndex *A3i, MatrixIndex *A3j, Value **A3x,						// Input A matrix in triplet form
		MatrixIndex rowCount, MatrixIndex colCount, ValueIndex valueCount,		// Input A matrix dimensions

		ValueIndex **Loutp, MatrixIndex **Louti, Value ***Loutx,				// Output L matrix in column compressed form, allocated inside
		ValueIndex **Uoutp, MatrixIndex **Uouti, Value ***Uoutx,				// Output U matrix in row compressed form, allocated inside
		MatrixIndex **rowPermOut, MatrixIndex **rowPermInvOut,					// Output Row permutation both ways, allocated inside
		MatrixIndex **colPermOut, MatrixIndex **colPermInvOut					// Output Column permutation both ways, allocated inside

);

#endif /* FINITESPARSELU_H_ */
