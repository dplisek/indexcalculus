/*
 * FactorFrontal.h
 *
 *  Created on: Feb 24, 2017
 *      Author: plech
 */

#ifndef FACTORFRONTAL_H_
#define FACTORFRONTAL_H_

#include "types.h"

int factorFrontalMatrix(TaskDescriptor *taskQueue, int taskCount);

#endif /* FACTORFRONTAL_H_ */
